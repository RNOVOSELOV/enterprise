package xyz.rnovoselov.enterprise.enterprise.data.managers;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by roman on 20.03.17.
 */
public class PreferencesManagerTest {

    @Mock
    SharedPreferences mockSharedPreferences;
    @Mock
    SharedPreferences.Editor mockEditor;
    @Mock
    Context mockContext;

    private Map<String, String> fakeStringMap = new HashMap<>();
    private Map<String, Boolean> fakeBooleanMap = new HashMap<>();
    private Map<String, Integer> fakeIntMap = new HashMap<>();

    private HashSet<String> fakeStringSet;
    private PreferencesManager mPreferencesManager;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        given(mockContext.getSharedPreferences(anyString(), anyInt())).willReturn(mockSharedPreferences);
        given(mockSharedPreferences.edit()).willReturn(mockEditor);

        mPreferencesManager = new PreferencesManager(mockContext);
    }

    private void preparePutBooleanStub() {
        given(mockEditor.putBoolean(anyString(), anyBoolean())).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            Boolean value = invocation.getArgument(1);
            fakeBooleanMap.put(key, value);
            return null;
        });

        given(mockSharedPreferences.getBoolean(anyString(), anyBoolean())).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            Boolean value = fakeBooleanMap.get(key);
            if (value == null) {
                value = invocation.getArgument(1);
            }
            return value;
        });
    }

    private void preparePutIntStub() {
        given(mockEditor.putInt(anyString(), nullable(Integer.class))).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            Integer value = invocation.getArgument(1);
            fakeIntMap.put(key, value);
            return null;
        });

        given(mockSharedPreferences.getInt(anyString(), nullable(Integer.class))).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            Integer value = fakeIntMap.get(key);
            if (value == null) {
                value = invocation.getArgument(1);
            }
            return value;
        });
    }

    private void preparePutStringSetStub() {
        given(mockEditor.putStringSet(anyString(), nullable(Set.class))).willAnswer(invocation -> {
            fakeStringSet = invocation.getArgument(1);
            return null;
        });

        given(mockSharedPreferences.getStringSet(anyString(), nullable(Set.class))).willAnswer(invocation -> {
            HashSet<String> value = fakeStringSet;
            if (value == null) {
                value = invocation.getArgument(1);
            }
            return value;
        });
    }

    private void preparePutStringStub() {
        given(mockEditor.putString(anyString(), nullable(String.class))).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            String value = invocation.getArgument(1);
            fakeStringMap.put(key, value);
            return null;
        });

        given(mockSharedPreferences.getString(anyString(), nullable(String.class))).willAnswer(invocation -> {
            String key = invocation.getArgument(0);
            String value = fakeStringMap.get(key);
            if (value == null) {
                value = invocation.getArgument(1);
            }
            return value;
        });
    }

    //region ================ Products ================

    @Test
    public void getLastProductUpdate_updateNotExist_DEFAULT_VALUE() throws Exception {
        //given
        preparePutStringStub();

        //when
        String actualLastUpdate = mPreferencesManager.getLastProductUpdate();

        //then
        assertEquals(PreferencesManager.DEFAULT_LAST_UPDATE_DATE, actualLastUpdate);
    }

    @Test
    public void getLastProductUpdate_updateExist_LAST_UPDATE_EQ_EXPECTED_DATE() throws Exception {
        //given
        preparePutStringStub();
        String expectedDate = "Sun Jan 19 2017 14:00:00 GMT+0000 (UTC)";
        mockEditor.putString(PreferencesManager.PRODUCT_LAST_UPDATE_KEY, expectedDate);

        //when
        String actualLastUpdate = mPreferencesManager.getLastProductUpdate();

        //then
        assertEquals(expectedDate, actualLastUpdate);
    }

    @Test
    public void saveLastProductUpdate_LAST_UPDATE_EQ_EXPECTED_DATE() throws Exception {
        preparePutStringStub();
        String expectedDate = "Sun Jan 19 2017 14:00:00 GMT+0000 (UTC)";

        mPreferencesManager.saveLastProductUpdate(expectedDate);

        verify(mockEditor, times(1)).apply();
        assertEquals(mockSharedPreferences.getString(PreferencesManager.PRODUCT_LAST_UPDATE_KEY, null), expectedDate);
    }

    //endregion

    //region ================ Token ================

    @Test
    public void getAuthToken_tokenNotExist() throws Exception {
        //given
        preparePutStringStub();

        //when
        String actualToken = mPreferencesManager.getAuthToken();

        //then
        assertNull(actualToken);
    }

    @Test
    public void getAuthToken_tokenExist_TOKEN_EQ_USER_TOKEN() throws Exception {
        //given
        preparePutStringStub();
        String expectedToken = "joihrtgbowithb[owithbwrhnoi9230840293rjk3p4oktjki-054t-gkr,g";
        mockEditor.putString(PreferencesManager.AUTH_TOKEN, expectedToken);

        //when
        String actualToken = mPreferencesManager.getAuthToken();

        //then
        assertEquals(expectedToken, actualToken);
    }

    @Test
    public void saveAuthToken_expectedToken_TOKEN_EQ_EXPETED_TOKEN() throws Exception {
        preparePutStringStub();
        String expectedToken = "joihrtgbowithb[owithbwrhnoi9230840293rjk3p4oktjki-054t-gkr,g";

        mPreferencesManager.saveAuthToken(expectedToken);

        verify(mockEditor, times(1)).apply();
        assertEquals(mockSharedPreferences.getString(PreferencesManager.AUTH_TOKEN, null), expectedToken);
    }

    //endregion

    //region ================ User ================

    @Test
    public void isUserAuth_tokenNotExist_DEF_VALUE_FALSE() throws Exception {
        preparePutBooleanStub();

        boolean actualResult = mPreferencesManager.isUserAuth();

        assertFalse(actualResult);
    }

    @Test
    public void isUserAuth_tokenExist_TRUE() throws Exception {
        preparePutStringStub();
        String expectedToken = "joihrtgbowithb[owithbwrhnoi9230840293rjk3p4oktjki-054t-gkr,g";
        mockEditor.putString(PreferencesManager.AUTH_TOKEN, expectedToken);

        boolean actualResult = mPreferencesManager.isUserAuth();

        assertTrue(actualResult);
    }

    @Test
    public void saveUserFullName_expectedName_EXPECTED_NAME_EQ_NAME() throws Exception {
        preparePutStringStub();
        String userName = "Roman Novoselov";

        mPreferencesManager.saveUserFullName(userName);

        verify(mockEditor, times(1)).apply();
        assertEquals(mockSharedPreferences.getString(PreferencesManager.PROFILE_FULL_MANE_KEY, null), userName);
    }

    @Test
    public void saveUserAvatar_expectedAvatar_EXPECTED_AVATAR_EQ_AVATAR() throws Exception {
        preparePutStringStub();
        String avatar = "android.resource://xyx.rnovoselov.enterprise.enterprise/drawable/avatar";

        mPreferencesManager.saveUserAvatar(avatar);

        verify(mockEditor, times(1)).apply();
        assertEquals(mockSharedPreferences.getString(PreferencesManager.PROFILE_AVATAR_KEY, null), avatar);
    }

    @Test
    public void saveUserPhone_expectedUserPhone_PHONE_EQ_EXPECTED_PHONE() throws Exception {
        preparePutStringStub();
        String phone = "+79027888883";
        String phoneWarning = "+790278888833";

        mPreferencesManager.saveUserPhone(phone);

        verify(mockEditor, times(1)).apply();
        assertEquals(mockSharedPreferences.getString(PreferencesManager.PROFILE_PHONE_KEY, null), phone);
        assertNotEquals(mockSharedPreferences.getString(PreferencesManager.PROFILE_PHONE_KEY, null), phoneWarning);
    }

    @Test
    public void getUserFullName() throws Exception {
        preparePutStringStub();
        String userName = "Roman Novoselov";
        mockEditor.putString(PreferencesManager.PROFILE_FULL_MANE_KEY, userName);

        String name = mPreferencesManager.getUserFullName();

        assertEquals(name, userName);
    }

    @Test
    public void getUserAvatar() throws Exception {
        preparePutStringStub();
        String avatar = "android.resource://xyx.rnovoselov.enterprise.enterprise/drawable/avatar";
        mockEditor.putString(PreferencesManager.PROFILE_AVATAR_KEY, avatar);

        String av = mPreferencesManager.getUserAvatar(mockContext);

        assertEquals(av, avatar);
    }

    @Test
    public void getUserPhone() throws Exception {
        preparePutStringStub();
        String phone = "+79027888883";
        mockEditor.putString(PreferencesManager.PROFILE_PHONE_KEY, phone);

        String expectedPhone = mPreferencesManager.getUserPhone();

        assertEquals(phone, expectedPhone);
    }

    //endregion

    //region ================ Settings ================

    @Test
    public void saveNotificationParameter() throws Exception {
        preparePutBooleanStub();
        boolean expectedValueOrder = false;
        boolean expectedValuePromo = true;

        mPreferencesManager.saveNotificationParameter(PreferencesManager.NOTIFICATION_ORDER_KEY, expectedValueOrder);
        mPreferencesManager.saveNotificationParameter(PreferencesManager.NOTIFICATION_PROMO_KEY, expectedValuePromo);

        verify(mockEditor, times(2)).apply();
        assertEquals(mockSharedPreferences.getBoolean(PreferencesManager.NOTIFICATION_ORDER_KEY, !expectedValueOrder), expectedValueOrder);
        assertEquals(mockSharedPreferences.getBoolean(PreferencesManager.NOTIFICATION_PROMO_KEY, !expectedValuePromo), expectedValuePromo);
    }

    @Test
    public void getOrderNotificationParameter() throws Exception {
        preparePutBooleanStub();
        boolean value = false;
        mockEditor.putBoolean(PreferencesManager.NOTIFICATION_ORDER_KEY, value);

        boolean expectedValue = mPreferencesManager.getOrderNotificationParameter();

        assertEquals(value, expectedValue);
    }

    @Test
    public void getPromoNotificationParameter() throws Exception {
        preparePutBooleanStub();
        boolean value = true;
        mockEditor.putBoolean(PreferencesManager.NOTIFICATION_PROMO_KEY, value);

        boolean expectedValue = mPreferencesManager.getPromoNotificationParameter();

        assertEquals(value, expectedValue);
    }

    //endregion

}