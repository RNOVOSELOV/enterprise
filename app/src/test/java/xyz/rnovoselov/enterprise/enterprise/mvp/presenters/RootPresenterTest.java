package xyz.rnovoselov.enterprise.enterprise.mvp.presenters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.view.ViewPager;

import com.squareup.leakcanary.RefWatcher;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ActivityResultDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.components.AppComponent;
import xyz.rnovoselov.enterprise.enterprise.di.components.DaggerAppComponent;
import xyz.rnovoselov.enterprise.enterprise.di.modules.AppModule;
import xyz.rnovoselov.enterprise.enterprise.di.modules.RootModule;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AccountModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IRootView;
import xyz.rnovoselov.enterprise.enterprise.resourses.StubEntityFactory;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.DaggerRootActivity_RootComponent;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.RootActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

/**
 * Created by roman on 04.04.17.
 */
public class RootPresenterTest {

    @Mock
    AccountModel mockAccountModel;
    @Mock
    private RootActivity mockRootView;
    @Mock
    Context mockContext;

    private RootPresenter mPresenter;
    private BundleServiceRunner mBundleServiceRunner;
    private MortarScope mMortarScope;
    private RootActivity.RootComponent mTestRootComponent;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        prepareDependency();    // Подготавливаем Dependency
        prepareScope();         // Подготавливаем Scope
        prepareRxSchedulers();  // Подготавливаем Rx Schedulers
        prepareStubs();         // Подготавливаем заглушки

        mPresenter = new RootPresenter();
    }

    private void prepareStubs() {
        //noinspection WrongConstant
        given(mockRootView.getSystemService(BundleServiceRunner.SERVICE_NAME)).willReturn(mBundleServiceRunner);
        //noinspection WrongConstant
        given(mockRootView.getSystemService(MortarScope.class.getName())).willReturn(mMortarScope);
        given(mockAccountModel.getUserInfoObs()).willReturn(BehaviorSubject.create());
    }


    private void prepareRxSchedulers() {
        RxAndroidPlugins.getInstance().reset();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
    }

    private void prepareScope() {
        mBundleServiceRunner = new BundleServiceRunner();
        mMortarScope = MortarScope.buildRootScope()
                .withService(BundleServiceRunner.SERVICE_NAME, mBundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, mTestRootComponent)
                .build("MockRoot");
    }

    private void prepareDependency() {
        AppComponent testAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext, mock(RefWatcher.class)))
                .build();
        mTestRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(testAppComponent)
                .rootModule(new RootModule() {
                    @Override
                    public RootPresenter provideRootPresenter() {
                        return mock(RootPresenter.class);   // Этот экз замокирован, т.к. он дергается из контекста вьюхи
                    }

                    @Override
                    public AccountModel provideAccountModel() {
                        return mockAccountModel;
                    }
                })
                .build();
    }

    @After
    public void tearDown() throws Exception {
        mPresenter.dropView(mockRootView);
    }

    @Test
    public void getRootView_takeView_INSTANCE_OF_ROOT_ACTIVITY() throws Exception {
        //given
        mPresenter.takeView(mockRootView);

        //when
        IRootView actualView = mPresenter.getRootView();

        //then
        assertTrue(actualView instanceof RootActivity);
    }

    @Test
    public void newActionBarBuilder_call_NEW_BUILDER_OBJECT() throws Exception {
        //given

        //when
        RootPresenter.ActionBarBuilder actionBuilder1 = mPresenter.newActionBarBuilder();
        RootPresenter.ActionBarBuilder actionBuilder2 = mPresenter.newActionBarBuilder();

        //then
        assertNotEquals(actionBuilder1, actionBuilder2);
    }

    @Test
    public void ActionBarSuilder_buildWithTabs_CALL_ACTIVITY_SET_TABLAYOUT() throws Exception {
        //given
        String expectedTitle = "Title";
        boolean expectedArrow = true;
        MenuItemHolder expectedAction1 = StubEntityFactory.makeStub(MenuItemHolder.class);
        MenuItemHolder expectedAction2 = StubEntityFactory.makeStub(MenuItemHolder.class);
        ArrayList<MenuItemHolder> expectedActions = new ArrayList<>();
        expectedActions.add(expectedAction1);
        expectedActions.add(expectedAction2);
        ViewPager mockTab = mock(ViewPager.class);
        mPresenter.takeView(mockRootView);

        //when
        RootPresenter.ActionBarBuilder actualBuilder = mPresenter.newActionBarBuilder();
        actualBuilder.setTitle(expectedTitle)
                .setBackArrow(expectedArrow)
                .addAction(expectedAction1)
                .addAction(expectedAction2)
                .setTab(mockTab)
                .build();

        //then
        then(mockRootView).should(times(1)).setTitle(expectedTitle);
        then(mockRootView).should(times(1)).setBackArrow(expectedArrow);
        then(mockRootView).should(times(1)).setMenuItem(expectedActions);
        then(mockRootView).should(times(1)).setTabLayout(mockTab);
    }

    @Test
    public void ActionBarSuilder_buildWithoutTabs_CALL_ACTIVITY_REMOVE_TABLAYOUT() throws Exception {
        //given
        String expectedTitle = "Title_2";
        boolean expectedArrow = false;
        MenuItemHolder expectedAction1 = StubEntityFactory.makeStub(MenuItemHolder.class);
        ArrayList<MenuItemHolder> expectedActions = new ArrayList<>();
        expectedActions.add(expectedAction1);
        mPresenter.takeView(mockRootView);

        //when
        RootPresenter.ActionBarBuilder actualBuilder = mPresenter.newActionBarBuilder();
        actualBuilder.setTitle(expectedTitle)
                .setBackArrow(expectedArrow)
                .addAction(expectedAction1)
                .build();

        //then
        then(mockRootView).should(times(1)).setTitle(expectedTitle);
        then(mockRootView).should(times(1)).setBackArrow(expectedArrow);
        then(mockRootView).should(times(1)).setMenuItem(expectedActions);
        then(mockRootView).should(times(1)).removeTabLayout();
    }


    @Test
    public void onActivityResult_stubActivityResult_NOT_COMPLETE_OBS() throws Exception {
        //given
        int expectedRequestCode = 1;
        int expectedResultCode = 1;
        Intent expectedIntent = mock(Intent.class);
        TestSubscriber<ActivityResultDto> subscriber = new TestSubscriber<>();
        mPresenter.takeView(mockRootView);

        //when
        PublishSubject<ActivityResultDto> subj = mPresenter.getActivityResultDtoObs();
        subj.subscribe(subscriber);
        mPresenter.onActivityResult(expectedRequestCode, expectedResultCode, expectedIntent);

        //then
        subscriber.assertNotCompleted();
        subscriber.assertNoErrors();

        ActivityResultDto actualActivityResult = subscriber.getOnNextEvents().get(0);
        assertEquals(expectedRequestCode, actualActivityResult.getRequestCode());
        assertEquals(expectedResultCode, actualActivityResult.getResultCode());
        assertEquals(expectedIntent, actualActivityResult.getIntent());
    }

    @Test
    public void checkPermissionsAndRequestIfNotGranted_permissionGranted_NOT_REQUEST_PERMISSION() throws Exception {
        //given
        int expectedRequestCode = 1;
        String[] expectedPermission = new String[]{
                "android.permission.WRITE_EXTERNAL_STORAGE",
                "android.permission.READ_EXTERNAL_STORAGE"
        };
        mPresenter.takeView(mockRootView);
        given(mockRootView.isAllGranted(expectedPermission, false)).willReturn(true);   // разрешения приняты

        //when
        mPresenter.checkPermissionsAndRequestIfNotGranted(expectedPermission, expectedRequestCode);

        //then
        then(mockRootView).should(never()).requestPermissions(expectedPermission, expectedRequestCode);
    }

    @Test
    public void checkPermissionsAndRequestIfNotGranted_permissionDenied_Sdk23_REQUEST_PERMISSION() throws Exception {
        //given
        int expectedRequestCode = 1;
        String[] expectedPermission = new String[]{
                "android.permission.WRITE_EXTERNAL_STORAGE",
                "android.permission.READ_EXTERNAL_STORAGE"
        };
        mPresenter.takeView(mockRootView);
        given(mockRootView.isAllGranted(expectedPermission, false)).willReturn(false);  // разрешения отклонены
        setFinalStatic(Build.VERSION.class.getField("SDK_INT"), 23);                    // Устанавливаем версию SDK

        //when
        mPresenter.checkPermissionsAndRequestIfNotGranted(expectedPermission, expectedRequestCode);

        //then
        then(mockRootView).should(times(1)).requestPermissions(expectedPermission, expectedRequestCode); // Запрос разрешений вызывается 1 раз
    }

    @Test
    public void checkPermissionsAndRequestIfNotGranted_permissionDenied_Sdk19_REQUEST_PERMISSION() throws Exception {
        //given
        int expectedRequestCode = 1;
        String[] expectedPermission = new String[]{
                "android.permission.WRITE_EXTERNAL_STORAGE",
                "android.permission.READ_EXTERNAL_STORAGE"
        };
        mPresenter.takeView(mockRootView);
        given(mockRootView.isAllGranted(expectedPermission, false)).willReturn(false);
        setFinalStatic(Build.VERSION.class.getField("SDK_INT"), 19);                    // Устанавливаем версию SDK

        //when
        mPresenter.checkPermissionsAndRequestIfNotGranted(expectedPermission, expectedRequestCode);

        //then
        then(mockRootView).should(never()).requestPermissions(expectedPermission, expectedRequestCode); // Запрос разрешений не вызывается
    }

    static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);
        Field modifiedField = Field.class.getDeclaredField("modifiers");
        modifiedField.setAccessible(true);
        modifiedField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        field.set(null, newValue);
    }

}