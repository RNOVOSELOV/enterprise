package xyz.rnovoselov.enterprise.enterprise.data.managers;

import com.squareup.moshi.Moshi;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import rx.observers.TestSubscriber;
import xyz.rnovoselov.enterprise.enterprise.data.network.RestService;
import xyz.rnovoselov.enterprise.enterprise.data.network.error.AccessError;
import xyz.rnovoselov.enterprise.enterprise.data.network.error.ApiError;
import xyz.rnovoselov.enterprise.enterprise.data.network.req.UserLoginReq;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserRes;
import xyz.rnovoselov.enterprise.enterprise.resourses.MockResponses;
import xyz.rnovoselov.enterprise.enterprise.utils.ConstantManager;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by roman on 18.03.17.
 */
public class DataManagerTest {

    private Retrofit testRetrofit;
    private RestService testRestService;
    private MockWebServer testMockWebServer;
    private DataManager testDataManager;
    private TestSubscriber<UserRes> testSubscriber;

    @Before
    public void setUp() throws Exception {
        testMockWebServer = new MockWebServer();
        testRetrofit = new Retrofit.Builder()
                .baseUrl(testMockWebServer.url("").toString())
                .addConverterFactory(MoshiConverterFactory.create(
                        new Moshi.Builder()
                                // Для того чтобы тестировать разбор ответа от сервера тут могут быть кастомные адаптеры
                                .build()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())   // для поддержки RX
                .client(new OkHttpClient.Builder()
                        // тут конфигурируем тестовый клиент при необходимости
                        .build())
                .build();
        testRestService = testRetrofit.create(RestService.class);
        testDataManager = new DataManager(testRestService);

        testSubscriber = new TestSubscriber<>();
    }

    @After
    public void tearDown() throws Exception {
        testMockWebServer.shutdown();
        testSubscriber.unsubscribe();
    }

    @Test(timeout = 800)
    public void loginUser_200_OK() throws Exception {
        MockResponse mockResponse = new MockResponse()
                .setHeader(ConstantManager.HEADER_LAST_MODIFIED, "Wed, 15 Nov 1995 04:58:08 GMT")
                .setBody(MockResponses.USER_RES_200)
                .throttleBody(512, 300, TimeUnit.MILLISECONDS); // Эмуляция скорости соединения
        testMockWebServer.enqueue(mockResponse);
        testDataManager.loginUser(new UserLoginReq("myemail@mail.ru", "mypassword"))
                .subscribe(userRes -> {
                    assertNotNull(userRes);
                    assertEquals("Вася", userRes.getFullName());
                }, throwable -> {
                    Assert.fail();
                });
    }

    @Test(expected = NullPointerException.class)
    public void exception_expected() throws Exception {
        testDataManager.getProductFromRealm();
    }

    @Test
    public void loginUser_200_OK_RX() throws Exception {
        MockResponse mockResponse = new MockResponse()
                .setHeader(ConstantManager.HEADER_LAST_MODIFIED, "Wed, 15 Nov 1995 04:58:08 GMT")
                .setBody(MockResponses.USER_RES_200);
        testMockWebServer.enqueue(mockResponse);
        testDataManager.loginUser(new UserLoginReq("myemail@mail.ru", "mypassword"))
                .subscribe(testSubscriber);
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertNoErrors();
    }

    @Test
    public void loginUser_403_FORBIDDEN() throws Exception {
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(403);

        testMockWebServer.enqueue(mockResponse);
        testDataManager.loginUser(new UserLoginReq("myemail@mail.ru", "mypassword"))
                .subscribe(userRes -> {
                    Assert.fail();
                }, throwable -> {
                    assertNotNull(throwable);
                    assertEquals("Неверный логин или пароль", throwable.getMessage());
                });
    }

    @Test
    public void loginUser_403_FORBIDDEN_RX() throws Exception {
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(403);

        testMockWebServer.enqueue(mockResponse);
        testDataManager.loginUser(new UserLoginReq("myemail@mail.ru", "mypassword"))
                .subscribe(testSubscriber);
        testSubscriber.assertNotCompleted();
        //testSubscriber.assertError(new Throwable());
        testSubscriber.assertError(AccessError.class);
    }

    @Test
    public void loginUser_500_UNKNOWN() throws Exception {
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(500);

        testMockWebServer.enqueue(mockResponse);
        testDataManager.loginUser(new UserLoginReq("myemail@mail.ru", "mypassword"))
                .subscribe(userRes -> {
                    Assert.fail();
                }, throwable -> {
                    assertNotNull(throwable);
                    assertEquals("Ошибка сервера 500", throwable.getMessage());
                });
    }

    @Test
    public void loginUser_500_UNKNOWN_RX() throws Exception {
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(500);

        testMockWebServer.enqueue(mockResponse);
        testDataManager.loginUser(new UserLoginReq("myemail@mail.ru", "mypassword"))
                .subscribe(testSubscriber);
        testSubscriber.assertNotCompleted();
        //testSubscriber.assertError(new Throwable());
        testSubscriber.assertError(ApiError.class);
    }
}