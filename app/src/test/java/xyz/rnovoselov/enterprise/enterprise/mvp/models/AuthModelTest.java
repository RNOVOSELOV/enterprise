package xyz.rnovoselov.enterprise.enterprise.mvp.models;

import com.birbit.android.jobqueue.JobManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.data.network.req.UserLoginReq;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;

/**
 * Created by roman on 16.03.17.
 */
public class AuthModelTest {

    @Mock
    DataManager mockDataManager;
    @Mock
    JobManager mockJobManager;

    private AuthModel model;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        model = new AuthModel(mockDataManager, mockJobManager);
    }

    @Test
    public void isUserAuth() throws Exception {
        model.isUserAuth();
        verify(mockDataManager, only()).isAuthUser();
    }

    @Test
    public void loginUser() throws Exception {
        model.loginUser("myemail@mail.ru", "mypassword");
        verify(mockDataManager, only()).loginUser(any(UserLoginReq.class));
    }

}