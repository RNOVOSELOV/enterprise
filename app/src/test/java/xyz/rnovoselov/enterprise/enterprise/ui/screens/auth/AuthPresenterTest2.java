package xyz.rnovoselov.enterprise.enterprise.ui.screens.auth;

import android.content.Context;

import com.squareup.leakcanary.RefWatcher;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.schedulers.Schedulers;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.components.AppComponent;
import xyz.rnovoselov.enterprise.enterprise.di.components.DaggerAppComponent;
import xyz.rnovoselov.enterprise.enterprise.di.modules.AppModule;
import xyz.rnovoselov.enterprise.enterprise.di.modules.RootModule;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AccountModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AuthModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.DaggerRootActivity_RootComponent;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.RootActivity;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.booleanThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

/**
 * Created by roman on 05.04.17.
 */
public class AuthPresenterTest2 {

    @Mock
    AccountModel mockAccountModel;
    @Mock
    RootPresenter mockRootPresenter;
    @Mock
    AuthView mockView;
    @Mock
    Context mockContext;
    @Mock
    AuthModel mockModel;
    @Mock
    RootActivity mockRootView;
    @Mock
    Flow mockFlow;
    @Mock(answer = Answers.RETURNS_SELF)
    RootPresenter.ActionBarBuilder mockActionBarBuilder;

    private AuthScreen.AuthPresenter mPresenter;
    private BundleServiceRunner mBundleServiceRunner;
    private MortarScope mMortarScope;
    private AuthScreen.Component mTestAuthComponent;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        prepareDependency();
        prepareScope();
        prepareRxSchedulers();
        prepareStubs();

        mPresenter = new AuthScreen.AuthPresenter();
    }

    private void prepareStubs() {
        //noinspection WrongConstant
        given(mockContext.getSystemService(BundleServiceRunner.SERVICE_NAME)).willReturn(mBundleServiceRunner);
        //noinspection WrongConstant
        given(mockContext.getSystemService(MortarScope.class.getName())).willReturn(mMortarScope);
        //noinspection WrongConstant
        given(mockContext.getSystemService("flow.InternalContextWrapper.FLOW_SERVICE")).willReturn(mockFlow);

        given(mockRootPresenter.getRootView()).willReturn(mockRootView);
        given(mockView.getContext()).willReturn(mockContext);
        given(mockModel.isUserAuth()).willReturn(false);

        given(mockRootPresenter.newActionBarBuilder()).willReturn(mockActionBarBuilder);
    }


    private void prepareRxSchedulers() {
        RxAndroidPlugins.getInstance().reset();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
    }

    private void prepareScope() {
        mBundleServiceRunner = new BundleServiceRunner();
        mMortarScope = MortarScope.buildRootScope()
                .withService(BundleServiceRunner.SERVICE_NAME, mBundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, mTestAuthComponent)
                .build("MockRoot");
    }

    private void prepareDependency() {
        AppComponent testAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext, mock(RefWatcher.class)))
                .build();
        RootActivity.RootComponent testRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(testAppComponent)
                .rootModule(new RootModule() {
                    @Override
                    public RootPresenter provideRootPresenter() {
                        return mockRootPresenter;
                    }

                    @Override
                    public AccountModel provideAccountModel() {
                        return mockAccountModel;
                    }
                })
                .build();

        mTestAuthComponent = DaggerAuthScreen_Component.builder()
                .rootComponent(testRootComponent)
                .module(new AuthScreen.Module() {
                    @Override
                    public AuthScreen.AuthPresenter providePresenter() {
                        return mock(AuthScreen.AuthPresenter.class);
                    }

                    @Override
                    public AuthModel provideAuthModel() {
                        return mockModel;
                    }
                })
                .build();
    }

    @Test
    public void clickOnLogin_isIdle_SHOW_LOGINWITH_ANIM() throws Exception {
        //given
        given(mockView.isIdle()).willReturn(true);
        mPresenter.takeView(mockView);

        //when
        mPresenter.onClickLogin();

        //then
        then(mockView).should(times(1)).showLoginWithAnim();
    }

    @Test
    public void clickOnLogin_notIdle_SIGN_IN_USER_REQUEST() throws Exception {
        //given
        String testEmail = "anymail@mail.ru";
        String testPassword = "anypassword";
        given(mockView.isIdle()).willReturn(false);
        mPresenter.takeView(mockView);
        given(mockView.getUserEmail()).willReturn(testEmail);
        given(mockView.getUserPassword()).willReturn(testPassword);
        given(mockModel.loginUser(testEmail, testPassword)).willReturn(Observable.empty());

//        Вдруг пригодится
//        given(mockView.getContext().getString(anyString())).willReturn(НЕОБХОИМАЯ СТРОКА, КОТОРАЯ В ПРОГРАММЕ БЕРЕТСЯ ИЗ РЕСУРСОВ);

        //when
        mPresenter.onClickLogin();

        //then
        then(mockModel).should(times(1)).loginUser(testEmail, testPassword);
        then(mockView).should(times(1)).hideLoginButton();
        then(mockView).should(times(1)).showIdleWithAnim();
    }

    @Test
    public void clickOnShowCatalog_anyAuthUser_OPEN_CATALOG_SCREEN() throws Exception {
        //given
        mPresenter.takeView(mockView);

        //when
        mPresenter.onClickShowCatalog();

        //then
        then(mockFlow).should(times(1)).goBack();
    }

    @Test
    public void isValidEmail_true() throws Exception
    {
        //given
        String expectedTarget = "sas@mail.ru";
        mPresenter.takeView(mockView);
//        given(mockView.getContext().getString(anyString())).willReturn(НЕОБХОИМАЯ СТРОКА, КОТОРАЯ В ПРОГРАММЕ БЕРЕТСЯ ИЗ РЕСУРСОВ);

        //when
        boolean actualresult = mPresenter.isValidEmail(expectedTarget);

        //then
        assertTrue(actualresult);
    }

    @Test
    public void isValidEmail_false() throws Exception
    {
        //given
        String expectedTarget = "sasmail.ru";
        String expectedTarget2 = "sas@mailru";
        mPresenter.takeView(mockView);
//        given(mockView.getContext().getString(anyString())).willReturn(НЕОБХОИМАЯ СТРОКА, КОТОРАЯ В ПРОГРАММЕ БЕРЕТСЯ ИЗ РЕСУРСОВ);

        //when
        boolean actualresult = mPresenter.isValidEmail(expectedTarget);
        boolean actualresult2 = mPresenter.isValidEmail(expectedTarget2);

        //then
        assertFalse(actualresult);
        assertFalse(actualresult2);
    }

    @Test
    public void onLoad_notAuthUser_SHOW_LOGIN_BTN() throws Exception {
        //given
        given(mockModel.isUserAuth()).willReturn(false);

        //when
        mPresenter.takeView(mockView);

        //then
        then(mockView).should().showLoginButton();
    }

    @Test
    public void onLoad_isAuthUser_HIDE_LOGIN_BTN() throws Exception {
        //given
        given(mockModel.isUserAuth()).willReturn(true);

        //when
        mPresenter.takeView(mockView);

        //then
        then(mockView).should().hideLoginButton();
    }

    @Test
    public void initActionbar_onLoad_SUCCESS_BUILD() throws Exception {
        //given
        String expectedTitle = "Авторизация";
        boolean expectedArrow = true;
//        given(mockRootPresenter.newActionBarBuilder()).willCallRealMethod();

        //when
        mPresenter.takeView(mockView);

        //then
        then(mockRootView).should().setTitle(expectedTitle);
        then(mockRootView).should().setBackArrow(expectedArrow);
    }
}