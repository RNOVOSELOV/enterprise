package xyz.rnovoselov.enterprise.enterprise.data.managers;

import com.squareup.moshi.Moshi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;
import xyz.rnovoselov.enterprise.enterprise.data.network.RestService;
import xyz.rnovoselov.enterprise.enterprise.data.network.error.ApiError;
import xyz.rnovoselov.enterprise.enterprise.data.network.error.ForbiddenApiError;
import xyz.rnovoselov.enterprise.enterprise.data.network.req.UserLoginReq;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.CommentJsonAdapter;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.ProductRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserRes;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;
import xyz.rnovoselov.enterprise.enterprise.resourses.StubEntityFactory;
import xyz.rnovoselov.enterprise.enterprise.resourses.TestResponses;
import xyz.rnovoselov.enterprise.enterprise.utils.ConstantManager;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

/**
 * Created by roman on 02.04.17.
 */
public class DataManagerTest2 {

    private MockWebServer mMockWebServer;
    private RestService mRestService;
    private DataManager mDataManager;
    private Retrofit mRetrofit;

    @Before
    public void setUp() throws Exception {
        prepareMockServer();
        mRestService = mRetrofit.create(RestService.class);
        prepareRxSchedulers();  // для переопределения Schedulers Rx (subscribeOn / observeOn)
    }

    //region ================ Prepare ================

    private void prepareRxSchedulers() {
        RxAndroidPlugins.getInstance().reset();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();  // без этого AndroidScheduler.mainThread -> NPE
            }
        });
    }

    private void prepareMockServer() {
        mMockWebServer = new MockWebServer();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(mMockWebServer.url("").toString())
                .addConverterFactory(MoshiConverterFactory.create(new Moshi.Builder()
                        .add(new CommentJsonAdapter())
                        .build()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();
    }

    /**
     * Метод возвращает тестовые респонсы от мок-сервера
     */
    private void prepareDispatcher_200() {
        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                String path = request.getPath();        // Получаем path  запроса
                switch (path) {
                    case "/login":                      // Path это то что в RestService и бэкслэш перед ним
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(TestResponses.SUCSESS_USER_RES_WITH_ADDRESS);
                    case "/products":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setHeader(ConstantManager.HEADER_LAST_MODIFIED, PreferencesManager.DEFAULT_LAST_UPDATE_DATE)
                                .setBody(TestResponses.SUCCSESS_GET_PRODUCTS);
                    default:
                        return new MockResponse()
                                .setResponseCode(404);
                }
            }
        };
        mMockWebServer.setDispatcher(dispatcher);
    }

    //endregion

    @After
    public void tearDown() throws Exception {
        mMockWebServer.shutdown();
    }

    @Test
    public void loginUser_200_SUCCESS_USER_RES() throws Exception {
        // given
        prepareDispatcher_200();        // Устанавливаем диспетчер запросов
        // Мокируем преференсес менеджер сюда сохраняется дата последнего обновления сущности (пригодится при тестировании товаров)
        PreferencesManager mockPrefManager = mock(PreferencesManager.class);
        mDataManager = new DataManager(mRestService, mockPrefManager, null); // Создаем DataManager
        UserLoginReq stubUserLogin = StubEntityFactory.makeStub(UserLoginReq.class); // Создаем заглушку с тестовыми данными на авторизацию пользователя
        UserRes expectedUserRes = StubEntityFactory.makeStub(UserRes.class); // Ожидаемый объект из запроса
        TestSubscriber<UserRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.loginUser(stubUserLogin)
                .subscribe(subscriber);     // Подписываемся тестовым сабскрайбером
        subscriber.awaitTerminalEvent();    // Дожидаемся окончания последовательности
        UserRes actualRes = subscriber.getOnNextEvents().get(0);    // Получаем первый и единственный элемент последовательности

        //then
        subscriber.assertNoErrors();
        assertEquals(expectedUserRes.getFullName(), actualRes.getFullName()); // Проверяем значения полей ожидаемого и фактического обьектов
        assertEquals(expectedUserRes.getAvatarUrl(), actualRes.getAvatarUrl());
        assertEquals(expectedUserRes.getId(), actualRes.getId());
        assertEquals(expectedUserRes.getPhone(), actualRes.getPhone());
        assertEquals(expectedUserRes.getToken(), actualRes.getToken());
        assertFalse(actualRes.getAddresses().isEmpty());
        then(mockPrefManager).should(times(1)).saveProfileInfo(actualRes);
    }


    @Test
    public void loginUser_403_FORBIDDEN() throws Exception {
        // given
        mMockWebServer.enqueue(new MockResponse().setResponseCode(403));
        mDataManager = new DataManager(mRestService, null, null); // Создаем DataManager
        UserLoginReq stubUserLogin = StubEntityFactory.makeStub(UserLoginReq.class); // Создаем заглушку с тестовыми данными на авторизацию пользователя
        TestSubscriber<UserRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.loginUser(stubUserLogin)
                .subscribe(subscriber);     // Подписываемся тестовым сабскрайбером
        subscriber.awaitTerminalEvent();    // Дожидаемся окончания последовательности
        Throwable actualThrow = subscriber.getOnErrorEvents().get(0); // Получаем ошибку

        //then
        subscriber.assertError(ForbiddenApiError.class);
        assertEquals("Неверный логин или пароль", actualThrow.getMessage());
    }

    @Test
    public void loginUser_500_API_ERROR() throws Exception {
        // given
        mMockWebServer.enqueue(new MockResponse().setResponseCode(500));
        mDataManager = new DataManager(mRestService, null, null); // Создаем DataManager
        UserLoginReq stubUserLogin = StubEntityFactory.makeStub(UserLoginReq.class); // Создаем заглушку с тестовыми данными на авторизацию пользователя
        TestSubscriber<UserRes> subscriber = new TestSubscriber<>();

        //when
        mDataManager.loginUser(stubUserLogin)
                .subscribe(subscriber);     // Подписываемся тестовым сабскрайбером
        subscriber.awaitTerminalEvent();    // Дожидаемся окончания последовательности

        //then
        subscriber.assertError(ApiError.class);
    }

    @Test
    public void getProductFromNetwork_200_RECORD_RESPONCE_TO_REALM_MANAGER() throws Exception {
        // given
        prepareDispatcher_200();
        PreferencesManager mockPrefManager = mock(PreferencesManager.class);
        given(mockPrefManager.getLastProductUpdate()).willReturn(PreferencesManager.DEFAULT_LAST_UPDATE_DATE);
        RealmManager mockRealmManager = mock(RealmManager.class);
        mDataManager = new DataManager(mRestService, mockPrefManager, mockRealmManager);
        TestSubscriber<ProductRealm> subscriber = new TestSubscriber<>();

        // when
        mDataManager.getProductObsFromNetwork()
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();    // ждем окончания последовательности

        // then
        subscriber.assertNoErrors();    // без ошибок
        subscriber.assertNoValues();    // данные вставляются в реалм и возвращается пустая последовательность
        subscriber.assertCompleted();   // Последовательность завершена
        // Последняя дата обновления должна быть обновлена
        then(mockPrefManager).should(times(1)).saveLastProductUpdate(anyString());
        // Один неактивный товар должен быть удален
        then(mockRealmManager).should(times(2)).deleteFromRealm(any(), anyString());
        // Восемь товаров долно быть добавлено
        then(mockRealmManager).should(times(7)).saveProductResponceToRealm(any(ProductRes.class));
    }

    @Test
    public void getProductFromNetwork_304_NOT_RECORD_TO_REALM_MANAGER() throws Exception {
        // given
        mMockWebServer.enqueue(new MockResponse().setResponseCode(304));
        PreferencesManager mockPrefManager = mock(PreferencesManager.class);
        given(mockPrefManager.getLastProductUpdate()).willReturn(PreferencesManager.DEFAULT_LAST_UPDATE_DATE);
        RealmManager mockRealmManager = mock(RealmManager.class);
        mDataManager = new DataManager(mRestService, mockPrefManager, mockRealmManager);
        TestSubscriber<ProductRealm> subscriber = new TestSubscriber<>();

        // when
        mDataManager.getProductObsFromNetwork()
                .subscribe(subscriber);
        subscriber.awaitTerminalEvent();    // ждем окончания последовательности

        // then
        subscriber.assertNoErrors();    // без ошибок
        subscriber.assertNoValues();    // данные вставляются в реалм и возвращается пустая последовательность
        subscriber.assertCompleted();   // Последовательность завершена
        // ничего не удаляется
        then(mockRealmManager).should(never()).deleteFromRealm(any(), anyString());
        // ничего не добавляется
        then(mockRealmManager).should(never()).saveProductResponceToRealm(any(ProductRes.class));
    }
}