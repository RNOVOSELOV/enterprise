package xyz.rnovoselov.enterprise.enterprise.ui.screens.auth;

import android.content.Context;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AuthModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.SplashActivity;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by roman on 19.03.17.
 */
public class AuthPresenterTest {

    @Mock
    AuthView mockView;
    @Mock
    Context mockContext;
    @Mock
    AuthModel mockModel;
    @Mock
    RootPresenter mockRootPresenter;
    @Mock
    SplashActivity mockRootView;
    @Mock
    DataManager mockDataManager;

    private AuthScreen.AuthPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        // Так как работаем с мортар MVP
        BundleServiceRunner mockBundleServiceRunner = new BundleServiceRunner();
        MortarScope mockMortarScope = MortarScope.buildRootScope()
                .withService(BundleServiceRunner.SERVICE_NAME, mockBundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, mock(AuthScreen.Component.class))
                .build("MockScope");

        // *************************
        // Эти правила действуют для всех тестов ниже, без них метод takeView()  у презентера не будет работать

        // Когда у контекста запрашивается системный сервис с названием BundleServiceRunner.SERVICE_NAME возвратить мокированный mockBundleServiceRunner
        given(mockContext.getSystemService(BundleServiceRunner.SERVICE_NAME))
                .willReturn(mockBundleServiceRunner);
        // Когда запрашивается системный сервис с именем MortarScope вернуть замокированный MortarScope
        given(mockContext.getSystemService(MortarScope.class.getName()))
                .willReturn(mockMortarScope);
        // Когда у view презентера запрашивается контекст вернуть замокированный контекст
        given(mockView.getContext()).willReturn(mockContext);
        // Когда у RootPresenter запрашивается RootView вернуть замокированную RootView
        given(mockRootPresenter.getRootView()).willReturn(mockRootView);
        // *************************

        // Создаем презентер с мокированной моделью и мокированным рут презентером
        mPresenter = new AuthScreen.AuthPresenter(mockModel, mockRootPresenter);
    }

    @Test
    public void onLoad_never_SHOW_ERROR() throws Exception {
        mPresenter.takeView(mockView);
        verify(mockRootView, never()).showError(any(Throwable.class));
    }

    @Test
    public void onLoad_isAuthUser_HIDE_LOGIN_BTN() throws Exception {
        given(mockModel.isUserAuth()).willReturn(true);
        mPresenter.takeView(mockView);
        verify(mockRootView, never()).showError(any(Throwable.class));
        verify(mockView, atMost(1)).hideLoginButton();
    }

    @Test
    public void onLoad_notAuthUser_SHOW_LOGIN_BTN() throws Exception {
        given(mockModel.isUserAuth()).willReturn(false);
        mPresenter.takeView(mockView);
        verify(mockRootView, never()).showError(any(Throwable.class));
        verify(mockView, atMost(1)).showLoginButton();
    }

    @Test
    public void onClickLogin_isIdle_SHOW_LOGIN_WITH_ANIM() throws Exception {
        mPresenter.takeView(mockView);
        given(mockView.isIdle()).willReturn(true);
        mPresenter.onClickLogin();
        verify(mockView).showLoginWithAnim();
    }

    @Test
    public void onClickLogin_notIdle_LOGIN_USER() throws Exception {
        mPresenter.takeView(mockView);
        given(mockView.isIdle()).willReturn(false);
        given(mockView.getUserEmail()).willReturn("novoselov_roman@mail.ru");
        given(mockView.getUserPassword()).willReturn("passwordpassword");
        mPresenter.onClickLogin();
        verify(mockModel).loginUser(any(String.class), any(String.class));
    }

    @Test
    public void onClickShowCatalog() throws Exception {
        mPresenter.takeView(mockView);
        mPresenter.onClickShowCatalog();
        verify(mockRootView).startRootActivity();
    }

    @Test
    public void checkUserAuth() throws Exception {
        mPresenter.checkUserAuth();
        verify(mockModel).isUserAuth();
    }

    @Test
    public void isValidEmail_true() throws Exception {
        assertTrue(mPresenter.isValidEmail("novoselov@mail.ru"));
    }

    @Test
    public void isValidEmail_false() throws Exception {
        assertFalse(mPresenter.isValidEmail("novoselov"));
        assertFalse(mPresenter.isValidEmail("novoselov@mail"));
    }

    @Test
    public void isValidPassword_true() throws Exception {
        assertTrue(mPresenter.isValidPassword("VerySafePassword"));
    }

    @Test
    public void isValidPassword_false() throws Exception {
        assertFalse(mPresenter.isValidEmail("pwd"));
    }

    @Test
    public void onClickFb() throws Exception {
        mPresenter.onClickFb();
        verify(mockRootView).showMessage("FACEBOOK");
    }

    @Test
    public void onClickVk() throws Exception {
        mPresenter.onClickVk();
        verify(mockRootView).showMessage("VK");
    }

    @Test
    public void onClickTwitter() throws Exception {
        mPresenter.onClickTwitter();
        verify(mockRootView).showMessage("TWITTER");
    }
}