package xyz.rnovoselov.enterprise.enterprise.resourses;

import android.view.MenuItem;

import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.network.req.UserLoginReq;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserRes;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserInfoDto;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.MenuItemHolder;

import static org.mockito.Mockito.mock;

/**
 * Created by Makweb on 19.03.2017.
 */

public class StubEntityFactory {

    @SuppressWarnings("unchecked")
    public static <T> T makeStub(Class<T> stubEntityClass) {
        switch (stubEntityClass.getSimpleName()) {
            case "UserRes":
                return (T) new UserRes("58711631a242690011b1b26d", "Макеев Михаил", "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWkuFxEM.jpg", "wegfvw;edcnw'lkedm93847983yuhefoij32lkml'kjvj30fewoidvn", "89179711111", null);
            case "UserLoginReq":
                return (T) new UserLoginReq("anymail@mail.ru", "password");
            case "UserInfoDto":
                return (T) new UserInfoDto("Макеев Михаил", "89179711111", "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWkuFxEM.jpg");
            case "MenuItemHolder":
                return (T) new MenuItemHolder("Редактировать", R.drawable.ic_account_circle_black_24dp, mock(MenuItem.OnMenuItemClickListener.class));
            default:
                return null;
        }
    }
}
