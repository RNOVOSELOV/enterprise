package xyz.rnovoselov.enterprise.enterprise.di.modules;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;

import dagger.Module;
import dagger.Provides;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.utils.AppConfig;

/**
 * Created by roman on 21.02.17.
 */

@Module
public class FlavorModelModule {

    @Provides
    JobManager provideJobManager() {
        Configuration configuration = new Configuration.Builder(DataManager.getInstance().getContext())
                .minConsumerCount(AppConfig.JOB_MIN_CONSUMER_COUNT) // Минимальное число потоков, которое будет держать JobManager
                .maxConsumerCount(AppConfig.JOB_MAX_CONSUMER_COUNT) // Максимальное число потоков, котрое будет держать JobManager
                .loadFactor(AppConfig.JOB_THREAD_LOAD_FACTOR)       // Число задач на один поток
                .consumerKeepAlive(AppConfig.JOB_THREAD_KEEP_ALIVE) // Время жизни неиспользуемого треда при пустой очереди задач
                .build();
        return new JobManager(configuration);
    }

}
