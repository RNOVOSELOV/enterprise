package xyz.rnovoselov.enterprise.enterprise.utils;

import xyz.rnovoselov.enterprise.enterprise.BuildConfig;

/**
 * Created by roman on 22.10.16.
 */

public interface ConstantManager {
    String TAG_PREFIX = "ENTERPRISE_";

    String PROGRESS_DIALOG_SHOWN = "PROGRESS_DIALOG_SHOWN_FLAG";

    String FILE_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".provider";

    int REQUEST_PROFILE_PHOTO_PICKER = 1000;
    int REQUEST_PROFILE_PHOTO_CAMERA = 1001;

    int REQUEST_PERMISSION_CAMERA = 2000;
    int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 2001;

    String HEADER_LAST_MODIFIED = "Last-Modified";
    String HEADER_IF_MODIFIED_SINCE = "If-Modified-Since";
}
