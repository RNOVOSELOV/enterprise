package xyz.rnovoselov.enterprise.enterprise.utils;

/**
 * Created by roman on 22.10.16.
 */

public interface AppConfig {
    String BASE_URL = "https://skba1.mgbeta.ru/api/v1/";
    String REG_EXP_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
            "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    String REG_EXP_PASSWORD = "^\\S{8,}$";
    int MAX_CONNECTION_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;
    int MAX_WRITE_TIMEOUT = 5000;
    int JOB_MIN_CONSUMER_COUNT = 1;
    int JOB_MAX_CONSUMER_COUNT = 3;
    int JOB_THREAD_LOAD_FACTOR = 3;
    int JOB_THREAD_KEEP_ALIVE = 120;
    int INITIAL_BACK_OFF_IN_MS = 1000;
    int UPDATE_DATA_UNTERVAL = 30;      // update data every 30 seconds
    int RETRY_REQUEST_COUNT = 3;
    int RETRY_REQUEST_BASE_DELAY = 1000;
}
