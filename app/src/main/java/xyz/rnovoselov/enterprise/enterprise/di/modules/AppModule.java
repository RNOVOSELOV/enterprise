package xyz.rnovoselov.enterprise.enterprise.di.modules;

import android.content.Context;

import com.squareup.leakcanary.RefWatcher;

import dagger.Module;
import dagger.Provides;

/**
 * Created by roman on 27.11.16.
 */

@Module
public class AppModule {

    private Context mContext;
    private RefWatcher mRefWatcher;

    public AppModule(Context context, RefWatcher refWatcher) {
        mContext = context;
        mRefWatcher = refWatcher;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }

    @Provides
    RefWatcher provideRefWatcher() {
        return mRefWatcher;
    }
}
