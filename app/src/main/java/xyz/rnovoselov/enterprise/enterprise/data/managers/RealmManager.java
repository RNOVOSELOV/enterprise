package xyz.rnovoselov.enterprise.enterprise.data.managers;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.CommentRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.ProductRes;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.CommentRealm;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;

/**
 * Created by roman on 06.02.17.
 */

public class RealmManager {

    private Realm mRealmInstance;

    public void saveProductResponceToRealm(ProductRes productRes) {
        Realm realm = Realm.getDefaultInstance();

        ProductRealm productRealm = new ProductRealm(productRes);

        if (!productRes.getComments().isEmpty()) {
            Observable.from(productRes.getComments())
                    .doOnNext(commentRes -> {
                        if (!commentRes.isActive()) {
                            deleteFromRealm(CommentRealm.class, commentRes.getId());    // удаляем комментарии из БД если неактивны
                        }
                    })
                    .filter(CommentRes::isActive)
                    .map(CommentRealm::new)                                             // перобразовываем в RealmObject
                    .subscribe(commentRealm -> productRealm.getCommentRealms().add(commentRealm));  // добавляем в ProductRealm
        }

        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(productRealm));        // добавляем или обновляем продукт в транзакции
        realm.close();
    }

    public void deleteFromRealm(Class<? extends RealmObject> entityRealmClass, String id) {
        Realm realm = Realm.getDefaultInstance();
        RealmObject entity = realm.where(entityRealmClass).equalTo("id", id).findFirst();

        if (entity != null) {
            realm.executeTransaction(realm1 -> entity.deleteFromRealm());
            realm.close();
        }
    }

    public Observable<ProductRealm> getAllProductsFromRealm () {
        RealmResults<ProductRealm> manageProduct = getQueryRealmInstance().where(ProductRealm.class).findAllAsync();
        return manageProduct
                .asObservable()                     // получаем RealmResults как Observable
                .filter(RealmResults::isLoaded)     // hotObservable!!! получаем только загруженные результаты ОЧЕНЬ ВАЖНО иначе непредсказуемое поведение так как горячий обсервабл
//                .first()                            // если нужна холодная последовательность ХАК
                .flatMap(Observable::from);         // преобразуем список в последовательность Observable<ProductRealm>
    }

    private Realm getQueryRealmInstance() {
        if (mRealmInstance == null || mRealmInstance.isClosed()) {
            mRealmInstance = Realm.getDefaultInstance();
        }
        return mRealmInstance;
    }
}
