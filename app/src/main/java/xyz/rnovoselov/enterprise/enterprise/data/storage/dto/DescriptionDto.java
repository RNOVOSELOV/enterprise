package xyz.rnovoselov.enterprise.enterprise.data.storage.dto;

import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;

/**
 * Created by roman on 12.02.17.
 */
public class DescriptionDto {

    private String description;
    private float rating;
    private int count;
    private int price;
    private boolean favorite;

    public DescriptionDto(ProductRealm mProduct) {
        this.description = mProduct.getDescription();
        this.rating = mProduct.getRating();
        this.count = mProduct.getCount();
        this.price = mProduct.getPrice();
        this.favorite = mProduct.isFavorite();
    }

    public String getDescription() {
        return description;
    }

    public float getRating() {
        return rating;
    }

    public int getCount() {
        return count;
    }

    public int getPrice() {
        return price;
    }

    public boolean isFavorite() {
        return favorite;
    }
}
