package xyz.rnovoselov.enterprise.enterprise.data.network.error;

/**
 * Created by roman on 18.03.17.
 */

public class AccessError extends Exception {
    public AccessError() {
        super("Неверный логин или пароль");
    }
}
