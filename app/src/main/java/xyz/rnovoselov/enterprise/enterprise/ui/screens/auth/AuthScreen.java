package xyz.rnovoselov.enterprise.enterprise.ui.screens.auth;

import android.os.Bundle;

import com.squareup.leakcanary.RefWatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.AuthScope;
import xyz.rnovoselov.enterprise.enterprise.flow.AbstractScreen;
import xyz.rnovoselov.enterprise.enterprise.flow.Screen;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AuthModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.IAuthPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.RootActivity;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.SplashActivity;
import xyz.rnovoselov.enterprise.enterprise.utils.AppConfig;

/**
 * Created by roman on 30.11.16.
 */

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = 1;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int mCustomState) {
        this.mCustomState = mCustomState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentRootComponent) {
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentRootComponent)
                .module(new Module())
                .build();
    }

    //region ================================= DI =================================

    @dagger.Module
    public static class Module {

        @Provides
        @AuthScope
        public AuthPresenter providePresenter() {
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
        public AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AuthScope
    public interface Component {
        void inject(AuthPresenter presenter);

        void inject(AuthView view);
    }

    //endregion

    //region ============================== Presenter =============================
    public static class AuthPresenter extends AbstractPresenter<AuthView, AuthModel> implements IAuthPresenter {

        @Inject
        RefWatcher mRefWatcher;

        public AuthPresenter() {
            super();
        }

        //for test
        public AuthPresenter(AuthModel authModel, RootPresenter rootPresenter) {
            mModel = authModel;
            mRootPresenter = rootPresenter;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            final AuthView view = getView();
            if (view != null) {
                if (checkUserAuth()) {
                    view.hideLoginButton();
                } else {
                    view.showLoginButton();
                }
            } else {
                getRootView().showError(new NullPointerException("Что то пошло не так"));
            }
        }

        @Override
        protected void onExitScope() {
            mRefWatcher.watch(this);
            super.onExitScope();
        }

        @Override
        protected void initActionBar() {
            if (getRootView() instanceof RootActivity) {
                mRootPresenter.newActionBarBuilder()
                        .setTitle("Авторизация")
                        .setBackArrow(true)
                        .build();
                getView().setDefaultBackground();
            }
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void onClickLogin() {
            final AuthView view = getView();
            if (view != null && getRootView() != null) {
                if (view.isIdle()) {
                    view.showLoginWithAnim();
                } else {
                    String email = view.getUserEmail();
                    String pwd = view.getUserPassword();
                    if (isValidPassword(pwd) && isValidEmail(email)) {
                        loginUser(email, pwd);
                    } else {
                        view.invalidLoginAnimation();
                        getRootView().showMessage("Email или пароль в неверном формате");
                    }
                }
            }
        }


        private void loginUser(String userEmail, String userPassword) {
            mModel.loginUser(userEmail, userPassword)
                    .subscribe(userRes -> {

                            },
                            throwable -> {
                                getRootView().showError(throwable);
                            }, this::onLoginSuccess);
        }

        @Override
        public void onClickFb() {
            if (getRootView() != null) {
                getRootView().showMessage("FACEBOOK");
            }
        }

        @Override
        public void onClickVk() {
            if (getRootView() != null) {
                getRootView().showMessage("VK");
            }
        }

        @Override
        public void onClickTwitter() {
            if (getRootView() != null) {
                getRootView().showMessage("TWITTER");
            }
        }

        @Override
        public void onClickShowCatalog() {
            if (getView() != null && getRootView() != null) {
                if (getRootView() instanceof SplashActivity) {
                    ((SplashActivity) getRootView()).startRootActivity();
//                    ((SplashActivity) getRootView()).overridePendingTransition();
                } else {
                    Flow.get(getView()).goBack();
                }
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mModel.isUserAuth();
        }

        private void onLoginSuccess() {
            if (getView() != null && getRootView() != null) {
                getRootView().showMessage(getView().getContext().getString(R.string.autotentification_successful));
                getView().hideLoginButton();
                getView().showIdleWithAnim();
            }
        }

        public boolean isValidEmail(CharSequence email) {
            if (getView() != null) {
                Pattern pattern = Pattern.compile(AppConfig.REG_EXP_EMAIL);
                Matcher matcher = pattern.matcher(email);
                return matcher.matches();
            } else {
                return false;
            }
        }

        public boolean isValidPassword(CharSequence password) {
            if (getView() != null) {
                Pattern pattern = Pattern.compile(AppConfig.REG_EXP_PASSWORD);
                Matcher matcher = pattern.matcher(password);
                return matcher.matches();
//                return password.length() > 8;
            } else {
                return false;
            }
        }
    }
//endregion
}
