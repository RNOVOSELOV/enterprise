package xyz.rnovoselov.enterprise.enterprise.ui.screens.product_details;

import android.os.Bundle;

import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.TreeKey;
import mortar.MortarScope;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.DaggerScope;
import xyz.rnovoselov.enterprise.enterprise.flow.AbstractScreen;
import xyz.rnovoselov.enterprise.enterprise.flow.Screen;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.DetailModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.MenuItemHolder;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.catalog.CatalogScreen;

/**
 * Created by roman on 28.01.17.
 */

@Screen(R.layout.screen_details)
public class DetailScreen extends AbstractScreen<CatalogScreen.Component> implements TreeKey {

    private final ProductRealm mProductRealm;

    public DetailScreen(ProductRealm mProduct) {
        mProductRealm = mProduct;
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerDetailScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new CatalogScreen();
    }

    //region ================ DI ================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(DetailScreen.class)
        DetailPresenter provideDetailPresenter() {
            return new DetailPresenter(mProductRealm);
        }

        @Provides
        @DaggerScope(DetailScreen.class)
        DetailModel provideDetailModel() {
            return new DetailModel();
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @DaggerScope(DetailScreen.class)
    public interface Component {
        void inject(DetailPresenter presenter);

        void inject(DetailView view);

        DetailModel getDetailModel();

        RootPresenter getRootPresenter();

        Picasso getPicasso();
    }

    //endregion

    public class DetailPresenter extends AbstractPresenter<DetailView, DetailModel> {

        private final ProductRealm mProduct;

        public DetailPresenter(ProductRealm productRealm) {
            mProduct = productRealm;
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setTitle(mProduct.getProductName())
                    .setBackArrow(true)
                    .addAction(new MenuItemHolder("В корзину", R.drawable.ic_shopping_basket_black_24dp, menuItem -> {
                        if (getRootView() != null) {
                            getRootView().showMessage("Перейти в корзину");
                        }
                        return true;
                    }))
                    .setTab(getView().getViewPager())
                    .build();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView(mProduct);

        }
    }
}
