package xyz.rnovoselov.enterprise.enterprise.data.network.res;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * Created by roman on 09.01.17.
 */
public class ProductRes {

    @Json(name = "_id")
    private String id;
    private int remoteId;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private float raiting;
    private boolean active;
    private List<CommentRes> comments;

    public int getRemoteId() {
        return remoteId;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public float getRaiting() {
        return raiting;
    }

    public boolean isActive() {
        return active;
    }

    public String getId() {
        return id;
    }

    public List<CommentRes> getComments() {
        return comments;
    }
}
