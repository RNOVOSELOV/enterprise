package xyz.rnovoselov.enterprise.enterprise.data.network.req;

/**
 * Created by roman on 18.03.17.
 */

public class UserLoginReq {
    private String login;
    private String password;

    public UserLoginReq(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
