package xyz.rnovoselov.enterprise.enterprise.data.storage.dto;

/**
 * Created by roman on 23.12.16.
 */

public class UserInfoDto {
    private String name;
    private String phone;
    private String avatar;

    public UserInfoDto(String name, String phone, String avatar) {
        this.name = name;
        this.phone = phone;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAvatar() {
        return avatar;
    }

    @Override
    public String toString() {
        return String.format("User:%s phone:%s avatar:%s", name, phone, avatar);
    }
}
