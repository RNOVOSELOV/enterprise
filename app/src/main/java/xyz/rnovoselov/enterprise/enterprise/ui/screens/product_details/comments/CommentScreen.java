package xyz.rnovoselov.enterprise.enterprise.ui.screens.product_details.comments;

import android.os.Bundle;

import java.util.List;

import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import mortar.MortarScope;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import xyz.rnovoselov.enterprise.enterprise.BuildConfig;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.CommentDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.CommentRealm;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.DaggerScope;
import xyz.rnovoselov.enterprise.enterprise.flow.AbstractScreen;
import xyz.rnovoselov.enterprise.enterprise.flow.Screen;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.DetailModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.product_details.DetailScreen;

/**
 * Created by roman on 09.02.17.
 */

@Screen(R.layout.screen_comments)
public class CommentScreen extends AbstractScreen<DetailScreen.Component> {

    private ProductRealm mProductRealm;

    public CommentScreen(ProductRealm productRealm) {
        mProductRealm = productRealm;
    }

    @Override
    public Object createScreenComponent(DetailScreen.Component parentComponent) {
        return DaggerCommentScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(CommentScreen.class)
        CommentsPresenter provideCommentsPresenter() {
            return new CommentsPresenter(mProductRealm);
        }
    }

    @dagger.Component(dependencies = DetailScreen.Component.class, modules = Module.class)
    @DaggerScope(CommentScreen.class)
    public interface Component {
        void inject(CommentsPresenter presenter);

        void inject(CommentView view);

        void inject(CommentsAdapter adapter);

    }

    public class CommentsPresenter extends AbstractPresenter<CommentView, DetailModel> {

        private final ProductRealm mProduct;
        private RealmChangeListener mListener;

        public CommentsPresenter(ProductRealm mProductRealm) {
            mProduct = mProductRealm;
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.showFab();
            mRootPresenter.setOnClickListenerOnFab(view -> clickOnAddComment());
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            mListener = new RealmChangeListener<ProductRealm>() {
                @Override
                public void onChange(ProductRealm element) {
                    updateProductList(element);
                }
            };

            mProduct.addChangeListener(mListener);

            RealmList<CommentRealm> comments = mProduct.getCommentRealms();
            Observable<CommentDto> commentsObs = Observable.from(comments)
                    .toSortedList((commentRealm, commentRealm2) -> {
                        if (commentRealm.getCommentDate().getTime() > commentRealm2.getCommentDate().getTime()) {
                            return -1;
                        } else if (commentRealm.getCommentDate().getTime() < commentRealm2.getCommentDate().getTime()) {
                            return 1;
                        }
                        return 0;
                    })
                    .flatMap(Observable::from)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .map(CommentDto::new);

            mCompSubs.add(subscribe(commentsObs, new ViewSubscriber<CommentDto>() {
                @Override
                public void onNext(CommentDto commentDto) {
                    getView().getAdapter().addItem(commentDto);
                }
            }));
            getView().initView();
        }

        @Override
        public void dropView(CommentView view) {
            mProduct.removeChangeListener(mListener);
            super.dropView(view);
        }

        private void updateProductList(ProductRealm element) {
            Observable<List<CommentDto>> observable = Observable.from(element.getCommentRealms())
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .toSortedList((commentRealm, commentRealm2) -> {
                        if (commentRealm.getCommentDate().getTime() > commentRealm2.getCommentDate().getTime()) {
                            return -1;
                        } else if (commentRealm.getCommentDate().getTime() < commentRealm2.getCommentDate().getTime()) {
                            return 1;
                        }
                        return 0;
                    })
                    .flatMap(Observable::from)
                    .map(CommentDto::new)
                    .toList();

            mCompSubs.add(subscribe(observable, new ViewSubscriber<List<CommentDto>>() {
                @Override
                public void onNext(List<CommentDto> commentDtos) {
                    getView().getAdapter().reloadAdapter(commentDtos);
                }
            }));
        }

        private void clickOnAddComment() {
            getView().showAddCommentDialog();
        }

        public void addComment(CommentRealm commentRealm) {
            switch (BuildConfig.FLAVOR) {
                case "base":
                    mModel.sendComment(mProduct.getId(), commentRealm);
                    break;
                case "realmMp":
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(realm1 -> mProduct.getCommentRealms().add(commentRealm));
                    realm.close();
                    break;
            }
        }
    }
}
