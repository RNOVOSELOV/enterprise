package xyz.rnovoselov.enterprise.enterprise.ui.screens.account;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserAddressDto;

/**
 * Created by roman on 10.12.16.
 */

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.AddressItemViewHolder> {

    private List<UserAddressDto> mAddresses = new ArrayList<>();

    @Override
    public AddressItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address_rv, parent, false);
        return new AddressItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddressItemViewHolder holder, int position) {
        holder.bindAddress(mAddresses.get(position));
    }

    @Override
    public int getItemCount() {
        return mAddresses.size();
    }

    public void addItem (UserAddressDto address) {
        mAddresses.add(address);
        notifyDataSetChanged();
    }

    public void reloadAdapter () {
        mAddresses.clear();
        notifyDataSetChanged();
    }

    public UserAddressDto getItem(int position) {
        return mAddresses.get(position);
    }

    public void removeItem(int position) {
        mAddresses.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mAddresses.size());
    }

    public class AddressItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.address_et)
        EditText addressItem;

        @BindView(R.id.comment_et)
        EditText commentItem;

        @BindView(R.id.address_til)
        TextInputLayout addressWrapper;

        public AddressItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindAddress(UserAddressDto address) {
            addressWrapper.setHint(address.getName());
            addressItem.setHint(address.getName());
            addressItem.setText(String.format("%s %s - %s, %d этаж",
                    address.getStreet(),
                    address.getHouse(),
                    address.getApartment(),
                    address.getFloor()));
            commentItem.setText(address.getComment());
        }
    }
}
