package xyz.rnovoselov.enterprise.enterprise.mvp.views;

import android.support.annotation.Nullable;

import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserInfoDto;

/**
 * Created by roman on 28.11.16.
 */

public interface IRootView extends IView {

    void showMessage(String message);

    void showError(Throwable error);

    void showLoad();

    void hideLoad();

    @Nullable
    IView getCurrentScreen();

    void initDrawer(UserInfoDto userInfoDto);
}
