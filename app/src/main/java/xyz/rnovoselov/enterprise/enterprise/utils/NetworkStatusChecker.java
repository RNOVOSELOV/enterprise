package xyz.rnovoselov.enterprise.enterprise.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import rx.Observable;
import xyz.rnovoselov.enterprise.enterprise.EnterpriseApplication;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;

/**
 * Created by roman on 10.01.17.
 */

public class NetworkStatusChecker {

    private static Boolean isNetworkAvailable() {
        Context context = DataManager.getInstance().getContext();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static Observable<Boolean> isInternetAvailable() {
        return Observable.just(isNetworkAvailable());
    }
}
