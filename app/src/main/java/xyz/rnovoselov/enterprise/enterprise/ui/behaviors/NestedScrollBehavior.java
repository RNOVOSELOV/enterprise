package xyz.rnovoselov.enterprise.enterprise.ui.behaviors;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.utils.AppHelpers;

/**
 * Created by roman on 06.12.16.
 */

public class NestedScrollBehavior extends AppBarLayout.ScrollingViewBehavior {

    private final int maxAppBarHeight;      // Максимальная высота AppBar
    private final int maxNsPadding;         // Максимальный паддинг NS

    /**
     * Конструктор бихейвера, чтобы можно было его использовать из xml разметки
     *
     * @param context контекст
     * @param attrs   набор аттрибутов
     */
    public NestedScrollBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        maxAppBarHeight = context.getResources().getDimensionPixelSize(R.dimen.size_collapsing_height);
        maxNsPadding = context.getResources().getDimensionPixelSize(R.dimen.padding_items_user_profile_86);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    /**
     * Метод вызывается каждый раз, когда изменяется вью (dependency), к которой привязан контролируемый (child),
     * а так же при прокрутке и появлении/исчезновении элементов
     *
     * @param parent     родительский {@link CoordinatorLayout }
     * @param child      контролируемое {@link View}, в нашем случае {@link ImageView}
     * @param dependency {@link View} от которого зависит контролируемое {@link View}, в нашем случае {@link android.support.v7.app.ActionBar}
     * @return
     */
    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        float currenrFriction = AppHelpers.currentFriction(0, maxAppBarHeight, dependency.getBottom());
        int calculatedPadding = AppHelpers.sizeFromFriction(0, maxNsPadding, currenrFriction);
        CoordinatorLayout.LayoutParams layoutParams = ((CoordinatorLayout.LayoutParams) child.getLayoutParams());
        layoutParams.height = AppBarLayout.LayoutParams.MATCH_PARENT;
        child.setLayoutParams(layoutParams);
        child.setPadding(0, calculatedPadding, 0, calculatedPadding);
        return super.onDependentViewChanged(parent, child, dependency);
    }
}