package xyz.rnovoselov.enterprise.enterprise.mvp.presenters;

/**
 * Created by roman on 22.10.16.
 */

public interface IAuthPresenter {

    void onClickLogin();

    void onClickShowCatalog();

    void onClickFb();

    void onClickVk();

    void onClickTwitter();

    boolean checkUserAuth();
}
