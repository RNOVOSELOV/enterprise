package xyz.rnovoselov.enterprise.enterprise.ui.custom_views;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.utils.AppHelpers;
import xyz.rnovoselov.enterprise.enterprise.utils.ViewHelper;

/**
 * Created by roman on 29.11.16.
 */

public class AspectRatioImageView extends AppCompatImageView {
    private static final float DEFAULT_ASPECT_RATIO = 1.73f;
    private final float mAspectRatio;
    private boolean mEnableAspect;

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mEnableAspect = true;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        mAspectRatio = typedArray.getFloat(R.styleable.AspectRatioImageView_aspect_ratio, DEFAULT_ASPECT_RATIO);
        typedArray.recycle();
    }

    public void enableAspectRatio(boolean enable) {
        mEnableAspect = enable;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // Запрещаем считать аспект и обрезать картинку
        // Например для случаев трансформации, когда картинка разворачивается во весь экран
        if (!mEnableAspect) {
            return;
        }

        int newWidth;
        int newHeight;
        if (getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            newWidth = getMeasuredWidth();
        } else {
            // В альбомном режиме ширина картинки это треть экрана
            Activity activity = AppHelpers.getActivityFromViewContext(getContext());
            if (activity != null) {
                newWidth = ViewHelper.getScreenWidthPx(activity) / 3;
            } else {
                newWidth = (int) (ViewHelper.getDensity(getContext()) * 50);
            }
        }
        newHeight = (int) (newWidth / mAspectRatio);
        setMeasuredDimension(newWidth, newHeight);
    }
}
