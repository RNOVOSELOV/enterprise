package xyz.rnovoselov.enterprise.enterprise.ui.behaviors;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.utils.AppHelpers;

/**
 * Created by roman on 06.12.16.
 */

public class UserAvatarBehavior extends AppBarLayout.ScrollingViewBehavior {

    private final int maxIserAvatar;        // Максимальная высота и ширина аватара пользователя
    private final int maxAppBarHeight;      // Максимальная высота AppBar
    private final int maxAvatarMargin;      // Максимальный маргин аватара

    /**
     * Конструктор бихейвера, чтобы можно было его использовать из xml разметки
     *
     * @param context контекст
     * @param attrs   набор аттрибутов
     */
    public UserAvatarBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        maxAppBarHeight = context.getResources().getDimensionPixelSize(R.dimen.size_collapsing_height);
        maxIserAvatar = context.getResources().getDimensionPixelSize(R.dimen.avatar_size_user_profile);
        maxAvatarMargin = context.getResources().getDimensionPixelSize(R.dimen.avatar_margin_profile);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    /**
     * Метод вызывается каждый раз, когда изменяется вью (dependency), к которой привязан контролируемый (child),
     * а так же при прокрутке и появлении/исчезновении элементов
     *
     * @param parent     родительский {@link CoordinatorLayout }
     * @param child      контролируемое {@link View}, в нашем случае {@link ImageView}
     * @param dependency {@link View} от которого зависит контролируемое {@link View}, в нашем случае {@link android.support.v7.app.ActionBar}
     * @return
     */
    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        float currenrFriction = AppHelpers.currentFriction(0, maxAppBarHeight, dependency.getBottom());
        int avatarSize = AppHelpers.sizeFromFriction(0, maxIserAvatar, currenrFriction);
        CoordinatorLayout.LayoutParams layoutParams = ((CoordinatorLayout.LayoutParams) child.getLayoutParams());
        layoutParams.height = avatarSize;
        layoutParams.width = avatarSize;
        layoutParams.topMargin = AppHelpers.sizeFromFriction(0, maxAvatarMargin, currenrFriction);
        layoutParams.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        child.setLayoutParams(layoutParams);
        return super.onDependentViewChanged(parent, child, dependency);
    }
}
