package xyz.rnovoselov.enterprise.enterprise.ui.screens.catalog;

import android.content.Context;
import android.os.Bundle;

import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Subscriber;
import rx.Subscription;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.DaggerScope;
import xyz.rnovoselov.enterprise.enterprise.flow.AbstractScreen;
import xyz.rnovoselov.enterprise.enterprise.flow.Screen;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.CatalogModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.ICatalogPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.MenuItemHolder;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.RootActivity;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.auth.AuthScreen;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.product.ProductScreen;

/**
 * Created by roman on 04.12.16.
 */

@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {
    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .module(new Module())
                .rootComponent(parentComponent)
                .build();
    }

    //region ========================== DI ==========================

    @dagger.Module
    public class Module {

        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(CatalogScreen.class)
    public interface Component {
        void inject(CatalogPresenter presenter);

        void inject(CatalogView view);

        CatalogModel getCatalogModel();

        RefWatcher getRefWatcher();

        Picasso getPicasso();

        RootPresenter getRootPresenter();
    }

    //endregion

    //region ======================= Presenter ======================

    public class CatalogPresenter extends AbstractPresenter<CatalogView, CatalogModel> implements ICatalogPresenter {

        @Inject
        RefWatcher mRefWatcher;
        private int lastPagerPosition;

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            mCompSubs.add(subscribeOnProductRealmObs());
        }

        @Override
        public void dropView(CatalogView view) {
            lastPagerPosition = getView().getCurrentPagerPosition();
            super.dropView(view);
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            mRootPresenter.newActionBarBuilder()
                    .setTitle("Каталог")
                    .addAction(new MenuItemHolder("В корзину", R.drawable.ic_shopping_basket_black_24dp, menuItem -> {
                        if (getRootView() != null) {
                            getRootView().showMessage("Перейти в корзину");
                        }
                        return true;
                    }))
                    .build();

            mRootPresenter.hideFab();
        }

        @Override
        protected void onExitScope() {
            mRefWatcher.watch(this);
            super.onExitScope();
        }

        @Override
        public void clickOnBuyButton(int position) {
            if (getView() != null) {
                if (checkUserAuth()) {
                    getView().getCurrentProductView().startAddToCardAnim();
                } else {
                    Flow.get(getView()).set(new AuthScreen());
                }
            }
        }

        private Subscription subscribeOnProductRealmObs() {
            if (getRootView() != null) {
                getRootView().showLoad();
            }
            return mModel.getProductObs()
                    .subscribe(new RealmSubscriber());
/*            return mModel.getProductObs()
                    .toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<List<ProductDto>>() {
                        @Override
                        public void onCompleted() {
                            getRootView().hideLoad();
                        }

                        @Override
                        public void onError(Throwable e) {
                            getRootView().hideLoad();
                            getRootView().showError(e);
                        }

                        @Override
                        public void onNext(List<ProductDto> productDtoList) {
                            getView().showCatalogView(productDtoList);
                        }
                    });
*/
        }

        @Override
        public boolean checkUserAuth() {
            return mModel.isUserAuth();
        }

        private class RealmSubscriber extends Subscriber<ProductRealm> {
            CatalogAdapter mAdapter = getView().getAdapter();

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (getRootView() != null) {
                    getRootView().showError(e);
                }
            }

            @Override
            public void onNext(ProductRealm productRealm) {
                mAdapter.addItem(productRealm);
                if (mAdapter.getCount() - 1 == lastPagerPosition) {
                    getRootView().hideLoad();
                    getView().showCatalogView();
                }
            }
        }

    }

    //endregion


    public static class Factory {
        public static Context createProductContext(ProductRealm product, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope = null;

            ProductScreen screen = new ProductScreen(product);
            String scopeName = String.format("%s_%s", screen.getScopeName(), product.getId());

            if (parentScope.findChild(scopeName) == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(DaggerService.<CatalogScreen.Component>getDaggerComponent(parentContext)))
                        .build(scopeName);

            } else {
                childScope = parentScope.findChild(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}