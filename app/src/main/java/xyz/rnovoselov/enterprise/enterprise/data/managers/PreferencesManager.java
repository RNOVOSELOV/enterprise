package xyz.rnovoselov.enterprise.enterprise.data.managers;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserRes;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ProductLocalInfo;

/**
 * Created by roman on 22.10.16.
 */

public class PreferencesManager {

    private static final String APP_PREFERENCES = "spreference";
    public static final String BASKET_COUNTER = "PREFF_BASKET_COUNTER";
    public static final String AUTH_TOKEN = "AUTH_TOKEN_KEY";
    public static final String PROFILE_FULL_MANE_KEY = "PROFILE_FULL_NAME_KEY";
    public static final String PROFILE_AVATAR_KEY = "PROFILE_AVATAR_KEY";
    public static final String PROFILE_PHONE_KEY = "PROFILE_PHONE_KEY";
    public static final String NOTIFICATION_ORDER_KEY = "NOTIFICATION_ORDER_KEY";
    public static final String NOTIFICATION_PROMO_KEY = "NOTIFICATION_PROMO_KEY";
    public static final String PRODUCT_LAST_UPDATE_KEY = "PRODUCT_LAST_UPDATE_KEY";
    public static final String DEFAULT_LAST_UPDATE_DATE = "Thu, 01 Jan 1970 00:00:00 GMT";

    private SharedPreferences mPreferences;

    private Map<Integer, ProductLocalInfo> mMockProductLocalInDb = new HashMap<>();

    public PreferencesManager(Context context) {
        mPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

    }

    /**
     * Сохранение в {@link SharedPreferences} времени последнего обновления товаров
     *
     * @param lastModified время, полученное с сервера
     */
    public void saveLastProductUpdate(String lastModified) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PRODUCT_LAST_UPDATE_KEY, lastModified);
        editor.apply();
    }

    /**
     * Получение из {@link SharedPreferences} времени последнего обновления товара
     *
     * @return время последнего обновления
     */
    @Nullable
    public String getLastProductUpdate() {
        return mPreferences.getString(PRODUCT_LAST_UPDATE_KEY, DEFAULT_LAST_UPDATE_DATE);
    }

    /**
     * Сохранение в {@link SharedPreferences} токена авторизации
     *
     * @param token токен, полученный с сервера
     */
    public void saveAuthToken(String token) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(AUTH_TOKEN, token);
        editor.apply();
    }

    /**
     * Получение из {@link SharedPreferences} сохраненного токена
     *
     * @return токен, либо пустая строка, если токен не обнаружен
     */
    @Nullable
    public String getAuthToken() {
        return mPreferences.getString(AUTH_TOKEN, null);
    }

    /**
     * Проверка на авторизацию пользователя
     *
     * @return true, если в SP записан токен пользователя
     */
    public boolean isUserAuth() {
        return mPreferences.getString(AUTH_TOKEN, null) != null;
    }

    /**
     * Сохранение в {@link SharedPreferences} имени клиента
     *
     * @param fullName имя клиента
     */
    public void saveUserFullName(String fullName) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PROFILE_FULL_MANE_KEY, fullName);
        editor.apply();
    }

    /**
     * Получение из {@link SharedPreferences} сохраненного имени пользователя
     *
     * @return сохраненное имя пользователя
     */
    @Nullable
    public String getUserFullName() {
        return mPreferences.getString(PROFILE_FULL_MANE_KEY, "Имя Фамилия");
    }

    /**
     * Сохранение в {@link SharedPreferences} пути до аватара пользователя
     *
     * @param avatarPath путь до аватара пользователя
     */
    public void saveUserAvatar(String avatarPath) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PROFILE_AVATAR_KEY, avatarPath);
        editor.apply();
    }

    /**
     * Получение из {@link SharedPreferences} сохраненного пути до аватара пользователя
     *
     * @return сохраненный путь до аватара
     */
    public String getUserAvatar(Context context) {
        String avatarUri = mPreferences.getString(PROFILE_AVATAR_KEY, "");
        if (avatarUri.isEmpty()) {
            int resId = R.drawable.avatar;
            Resources resources = context.getResources();
            avatarUri = ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resId) + '/' + resources.getResourceTypeName(resId) + '/' + resources.getResourceEntryName(resId);
        }
        return avatarUri;
    }

    public String getUserAvatar() {
        return getUserAvatar(DataManager.getInstance().getContext());
    }

    /**
     * Сохранение в {@link SharedPreferences} телефона клиента
     *
     * @param phone телефон клиента
     */
    public void saveUserPhone(String phone) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PROFILE_PHONE_KEY, phone);
        editor.apply();
    }

    /**
     * Получение из {@link SharedPreferences} сохраненного телефона пользователя
     *
     * @return сохраненный тлефон пользователя
     */
    @Nullable
    public String getUserPhone() {
        return mPreferences.getString(PROFILE_PHONE_KEY, "");
    }

    public void saveProfileInfo(UserRes userRes) {
        saveUserFullName(userRes.getFullName());
        saveUserAvatar(userRes.getAvatarUrl());
        saveAuthToken(userRes.getToken());
        saveUserPhone(userRes.getPhone());
    }

    /**
     * Сохранение в {@link SharedPreferences} параметра оповещения
     *
     * @param notification параметр
     */
    public void saveNotificationParameter(String key, boolean notification) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(key, notification);
        editor.apply();
    }

    /**
     * Получение из {@link SharedPreferences} параметра отправки оповещения о состоянии заказа
     *
     * @return параметр отправки оповещения
     */

    public boolean getOrderNotificationParameter() {
        return mPreferences.getBoolean(NOTIFICATION_ORDER_KEY, true);
    }

    /**
     * Получение из {@link SharedPreferences} параметра отправки оповещения о промо-акциях
     *
     * @return параметр отправки оповещения
     */

    public boolean getPromoNotificationParameter() {
        return mPreferences.getBoolean(NOTIFICATION_PROMO_KEY, true);
    }

    public ProductLocalInfo getLocalInfo(int remoteId) {
        return mMockProductLocalInDb.get(remoteId);
    }

    public void saveBasketCounter(int count) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putInt(BASKET_COUNTER, count);
        editor.apply();
    }

    public int getBacketCounter() {
        return mPreferences.getInt(BASKET_COUNTER, 0);
    }
}
