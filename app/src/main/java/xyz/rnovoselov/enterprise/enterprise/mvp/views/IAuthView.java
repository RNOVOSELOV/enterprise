package xyz.rnovoselov.enterprise.enterprise.mvp.views;

/**
 * Created by roman on 22.10.16.
 */

public interface IAuthView extends IView {

    void showLoginButton();

    void hideLoginButton();

    String getUserEmail();

    String getUserPassword();

    boolean isIdle();
}

