package xyz.rnovoselov.enterprise.enterprise.ui.screens.auth;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import flow.Flow;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.AbstractView;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IAuthView;
import xyz.rnovoselov.enterprise.enterprise.utils.ViewHelper;

/**
 * Created by roman on 30.11.16.
 */

public class AuthView extends AbstractView<AuthScreen.AuthPresenter> implements IAuthView {

    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    @BindView(R.id.login_auth_card)
    CardView mAuthCard;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.login_show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.login_fb_btn)
    Button mFbButton;
    @BindView(R.id.login_vk_btn)
    Button mVkButton;
    @BindView(R.id.login_tw_btn)
    Button mTwButton;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;
    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailWrapper;
    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswdWrapper;
    @BindView(R.id.login_company_name)
    TextView mCompanyName;
    @BindView(R.id.panel_wrapper)
    FrameLayout panelWrapper;
    @BindView(R.id.login_logo_img)
    ImageView mLogo;

    private Transition mBounds;
    private Transition mFade;
    private Animator scaleAnimator;

    private AuthScreen mAuthScreen;
    private int mDen;
    private Subscription mAnimObs;

    public AuthView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mDen = (int) ViewHelper.getDensity(context);
        initAnimVar();
    }

    private void initAnimVar() {
        mBounds = new ChangeBounds();
        mFade = new Fade();
        scaleAnimator = AnimatorInflater.loadAnimator(getContext(), R.animator.logo_scale_animator);
    }

    public void setDefaultBackground() {
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background));
    }

    @Override
    protected void initDagger(Context context) {
        mAuthScreen = Flow.getKey(this);
        DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void afterInflate() {
        showViewFromState();

        Typeface ptBebasNeueBookTypeface = Typeface.createFromAsset(this.getContext().getAssets(), "fonts/PTBebasNeueBook.ttf");
        mCompanyName.setTypeface(ptBebasNeueBookTypeface);

        initInputFields();

        LayoutTransition layoutTransition = new LayoutTransition();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        }
        this.setLayoutTransition(layoutTransition);
    }

    @Override
    protected void startInitAnimation() {
        super.startInitAnimation();
        startSocialAnimation();
        startLogoAnim();
    }

    @Override
    protected void beforeDrop() {
        super.beforeDrop();
        if (mAnimObs != null) {
            mAnimObs.unsubscribe();
        }
    }

    private void showViewFromState() {
        if (!isIdle()) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoginState() {
// Старая анимация
//        Animation animationShow = AnimationUtils.loadAnimation(this.getContext(), R.anim.auth_panel_in);
//        mAuthCard.startAnimation(animationShow);
        //mAuthCard.setVisibility(VISIBLE);
        //mShowCatalogBtn.setVisibility(GONE);

        CardView.LayoutParams cardParam = (CardView.LayoutParams) mAuthCard.getLayoutParams();  // Получаем текущие параметры макета
        cardParam.height = LinearLayout.LayoutParams.MATCH_PARENT;  // Устанавливаем высоты на высоту родителя
        mAuthCard.setLayoutParams(cardParam);                       // Устанавливаем параметры (requestLayout inside)
        mAuthCard.getChildAt(0).setVisibility(VISIBLE);             // input wrapper делаем невидимым
        mAuthCard.setCardElevation(4 * mDen);                       // Устанавливаем подъем карточки авторизации
        mShowCatalogBtn.setClickable(false);                        // Отключаем кликабельность кнопки входа в каталог
        mShowCatalogBtn.setVisibility(GONE);                        // Скрываем кнопку
        mAuthScreen.setCustomState(LOGIN_STATE);                        // Устанавливаем стэйт логин
    }

    private void showIdleState() {
        CardView.LayoutParams cardParam = (CardView.LayoutParams) mAuthCard.getLayoutParams();
        cardParam.height = ((int) (44 * mDen));         // Устанвливаем высоту катроки обратно в 44dp (высоту кнопки)
        mAuthCard.setLayoutParams(cardParam);
        mAuthCard.getChildAt(0).setVisibility(INVISIBLE);
        mAuthCard.setCardElevation(0f);
        mShowCatalogBtn.setClickable(true);
        mShowCatalogBtn.setVisibility(VISIBLE);
        mAuthScreen.setCustomState(IDLE_STATE);

        /*
        Animation animationHide = AnimationUtils.loadAnimation(this.getContext(), R.anim.auth_panel_out);
        animationHide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAuthCard.setVisibility(GONE);
                mShowCatalogBtn.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mAuthCard.startAnimation(animationHide);
        */
    }

    public void initInputFields() {
        mEmailEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().isEmpty() || mPresenter.isValidEmail(charSequence)) {
                    mEmailWrapper.setErrorEnabled(false);
                    mEmailWrapper.setError(null);
                } else {
                    mEmailWrapper.setError("Введен некорректный e-mail");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().isEmpty() || mPresenter.isValidPassword(charSequence)) {
                    mPasswdWrapper.setErrorEnabled(false);
                    mPasswdWrapper.setError(null);
                } else {
                    mPasswdWrapper.setError("Пароль должен быть не менее 8 символов");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void startSocialAnimation() {
        final Animation vkAnim = AnimationUtils.loadAnimation(this.getContext(), R.anim.social_button_in);
        final Animation twAnim = AnimationUtils.loadAnimation(this.getContext(), R.anim.social_button_in);
        final Animation fbAnim = AnimationUtils.loadAnimation(this.getContext(), R.anim.social_button_in);
        vkAnim.setStartOffset(150);
        twAnim.setStartOffset(300);
        fbAnim.setStartOffset(450);
        mVkButton.startAnimation(vkAnim);
        mTwButton.startAnimation(twAnim);
        mFbButton.startAnimation(fbAnim);
    }

    private void startSocialClickAnimation(View view) {
        final Animation animation = AnimationUtils.loadAnimation(this.getContext(), R.anim.social_button_pressed);
        view.startAnimation(animation);
    }

    //region ================================== Events =================================

    @OnClick(R.id.login_btn)
    void loginClick() {
        mPresenter.onClickLogin();
    }

    @OnClick(R.id.login_show_catalog_btn)
    void catalogClick() {
        mPresenter.onClickShowCatalog();
    }

    @OnClick(R.id.login_fb_btn)
    void fbClick() {
        mPresenter.onClickFb();
        startSocialClickAnimation(mFbButton);
    }

    @OnClick(R.id.login_tw_btn)
    void twitterClick() {
        mPresenter.onClickTwitter();
        startSocialClickAnimation(mTwButton);
    }

    @OnClick(R.id.login_vk_btn)
    void vkClick() {
        mPresenter.onClickVk();
        startSocialClickAnimation(mVkButton);
    }

    @OnClick(R.id.login_logo_img)
    void onLogoClick() {
        startLogoAnim();
    }

    //endregion

    //region ================================ IAuthView ================================

    @Override
    public void showLoginButton() {
        mLoginBtn.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoginButton() {
        mLoginBtn.setVisibility(GONE);
    }

    @Override
    public String getUserEmail() {
        return mEmailEt.getText().toString();
    }

    @Override
    public String getUserPassword() {
        return mPasswordEt.getText().toString();
    }

    @Override
    public boolean isIdle() {
        return mAuthScreen.getCustomState() == IDLE_STATE;
    }

    @Override
    public boolean viewOnBackPressed() {
        if (!isIdle()) {
            showIdleWithAnim();
            return true;
        } else {
            return false;
        }
    }

    //endregion

    //region ================ Animation ================

    public void invalidLoginAnimation() {

        Animator set0 = AnimatorInflater.loadAnimator(getContext(), R.animator.invalid_field_animator);
        set0.setTarget(mAuthCard);
        set0.start();

/*
        ObjectAnimator oa = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", 2f);
        ObjectAnimator ob = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", -2f);
        ObjectAnimator oc = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", 0f);

        AnimatorSet set = new AnimatorSet();
        set.playSequentially(oa, ob, oc);

        // Длительность анимации по умолчанию
        // на смартфонах 300 мс
        // на планшетах 375 мс
        // на часах ~ 200 мс
        set.setDuration(300);
        set.start();
*/
    }

    public void showLoginWithAnim() {
        TransitionSet set = new TransitionSet();
        set.addTransition(mBounds)      // анимируем положение и границы (высоту элемента и подьем)
                .addTransition(mFade)   // анимируем прозрачность (видимость элемента)
//                .setDuration(300)       // продолжтиельность анимации
                .setInterpolator(new FastOutSlowInInterpolator())       // устанавливаем временную функцию
                .setOrdering(TransitionSet.ORDERING_SEQUENTIAL);        // устанавливаем последовательность проигрывания анимаций при переходе
        TransitionManager.beginDelayedTransition(panelWrapper, set);    //
        showLoginState();
    }

    public void showIdleWithAnim() {
        TransitionSet set = new TransitionSet();

        Transition fade = new Fade();
        fade.addTarget(mAuthCard.getChildAt(0));    // Анимация исчезновения только для инпутов в

        set.addTransition(fade)
                .addTransition(mBounds)
                .addTransition(mFade)
//                .setDuration(5000)
                .setInterpolator(new FastOutSlowInInterpolator())
                .setOrdering(TransitionSet.ORDERING_SEQUENTIAL);
        TransitionManager.beginDelayedTransition(panelWrapper, set);
        showIdleState();
    }


    private void startLogoAnim() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawable = mLogo.getDrawable();
            if (drawable instanceof Animatable) {

                scaleAnimator.setTarget(mLogo);
                mAnimObs = Observable.interval(6000, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aLong -> {
                            scaleAnimator.start();
                            ((Animatable) drawable).start();
                        });
                ((Animatable) drawable).start();
            }
//            AnimatedVectorDrawableCompat avd = ((AnimatedVectorDrawable) AnimatedVectorDrawableCompat.create(getContext(), R.drawable.logo_vector_anim));
//            mLogo.setImageDrawable(avd);
//            avd.start();
        }
    }

    //endregion
}
