package xyz.rnovoselov.enterprise.enterprise.data.network.error;

/**
 * Created by roman on 10.01.17.
 */
public class NetworkUnavailableError extends Throwable {
    public NetworkUnavailableError() {
        super("Интернет недоступен, попробуйте позже");
    }
}
