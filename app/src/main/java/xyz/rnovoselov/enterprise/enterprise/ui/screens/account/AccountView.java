package xyz.rnovoselov.enterprise.enterprise.ui.screens.account;

import android.content.Context;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import flow.Flow;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserInfoDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserSettingsDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IAccountView;

/**
 * Created by roman on 04.12.16.
 */

public class AccountView extends CoordinatorLayout implements IAccountView {

    public static final int PREVIEW_STATE = 1;
    public static final int EDIT_STATE = 0;

    @Inject
    AccountScreen.AccountPresenter mPresenter;
    @Inject
    Picasso mPicasso;

    @BindView(R.id.profile_name_txt)
    TextView mProfileName;
    @BindView(R.id.user_avatar_img)
    CircleImageView mUserAvatar;
    @BindView(R.id.user_phone_et)
    EditText mUserPhone;
    @BindView(R.id.user_full_name_et)
    EditText mUserFullName;
    @BindView(R.id.profile_name_wrapper)
    LinearLayout mProfileNameWrapper;
    @BindView(R.id.address_list)
    RecyclerView mAddressesList;
    @BindView(R.id.addaddress_btn)
    Button mAddAddress;
    @BindView(R.id.notification_order_sw)
    SwitchCompat mNotificationOrder;
    @BindView(R.id.notification_promo_sw)
    SwitchCompat mNotificationPromo;

    private AddressListAdapter mAdapter;
    private AccountScreen mScreen;
    private UserDto mUserDto;
    private TextWatcher mWatcher;
    private Uri mAvatarUri;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    public AddressListAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
        Log.d("==== Callbacks", "onAttachedToWindow");
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
        Log.d("==== Callbacks", "onDetachedFromWindow");
    }

    private void showViewFromState() {
        if (mScreen.getCurrentState() == PREVIEW_STATE) {
            showPreviewState();
        } else {
            showEditState();
        }
    }

    public void initView() {
        showViewFromState();
        mAdapter = new AddressListAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mAddressesList.setLayoutManager(layoutManager);
        mAddressesList.setAdapter(mAdapter);
        initSwipe();
    }

    private void initSwipe() {
        ItemSwipeCallback swipeCallback = new ItemSwipeCallback(getContext(), 0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.LEFT) {
                    showRemoveAddressDialog(position);
                } else {
                    showEditAddressDialog(position);
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
        itemTouchHelper.attachToRecyclerView(mAddressesList);
    }

    private void showEditAddressDialog(final int position) {
        AlertDialog.Builder dialogBuider = new AlertDialog.Builder(getContext());
        dialogBuider.setTitle("Перейти к редактированию адреса")
                .setMessage("Вы уверены, что хотите изменить адрес доставки?")
                .setPositiveButton("Редактировать", (dialogInterface, i) -> mPresenter.editAddress(position))
                .setNegativeButton("Отмена", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mAdapter.notifyDataSetChanged();
                })
                .setOnCancelListener(dialogInterface -> mAdapter.notifyDataSetChanged())
                .show();
    }

    private void showRemoveAddressDialog(final int position) {
        AlertDialog.Builder dialogBuider = new AlertDialog.Builder(getContext());
        dialogBuider.setTitle("Удаление адреса")
                .setMessage("Вы уверены, что хотите удалить адрес доставки?")
                .setPositiveButton("Удалить", (dialogInterface, i) -> {
                    mPresenter.removeAddress(position);
                    mAdapter.notifyItemRemoved(position);
                    mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());
                })
                .setNegativeButton("Отмена", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    mAdapter.notifyDataSetChanged();
                })
                .setOnCancelListener(dialogInterface -> mAdapter.notifyDataSetChanged())
                .show();
    }

    public void initSettings(UserSettingsDto settings) {
        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mPresenter.switchSettings();
            }
        };

        mNotificationOrder.setChecked(settings.isOrderNotification());
        mNotificationPromo.setChecked(settings.isPromoNotification());

        mNotificationOrder.setOnCheckedChangeListener(listener);
        mNotificationPromo.setOnCheckedChangeListener(listener);
    }

    public UserSettingsDto getSettings() {
        return new UserSettingsDto(mNotificationOrder.isChecked(), mNotificationPromo.isChecked());
    }

    public void updateAvatarPhoto(Uri uri) {
        mAvatarUri = uri;
        insertAvatarPhoto();
    }

    private void insertAvatarPhoto() {
        mPicasso.load(mAvatarUri)
                .placeholder(R.drawable.avatar)
                .fit()
                .into(mUserAvatar);
    }

    public UserInfoDto getUserProfileInfo() {
        return new UserInfoDto(getUserName(), getUserPhone(), String.valueOf(mAvatarUri));
    }

    //region ================ IAccountView ================

    @Override
    public void changeState() {
        if (mScreen.getCurrentState() == PREVIEW_STATE) {
            mScreen.setCurrentState(EDIT_STATE);
        } else {
            mScreen.setCurrentState(PREVIEW_STATE);
        }
        showViewFromState();
    }

    @Override
    public void showEditState() {
        mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mProfileName.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        mUserFullName.addTextChangedListener(mWatcher);

        mProfileNameWrapper.setVisibility(VISIBLE);
        mUserFullName.requestFocus();
        mUserPhone.setEnabled(true);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            mUserPhone.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.grey_light)));
//            mUserFullName.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.grey_light)));
//        }
        mPicasso.load(R.drawable.ic_add_white_24dp)
                .error(R.drawable.ic_add_white_24dp)
                .into(mUserAvatar);

    }

    @Override
    public void showPreviewState() {
        mUserFullName.removeTextChangedListener(mWatcher);
        mProfileNameWrapper.setVisibility(GONE);
        mUserPhone.setEnabled(false);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            mUserPhone.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), android.R.color.transparent)));
//            mUserFullName.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), android.R.color.transparent)));
//        }
        mUserPhone.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
        if (mAvatarUri != null) {
            insertAvatarPhoto();
        }
    }

    @Override
    public void showPhotoSourceDialog() {
        String source[] = {"Загрузить из галлереи", "Сделать фото", "Отмена"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Установить фото");
        alertDialog.setItems(source, (dialogInterface, i) -> {
            switch (i) {
                case 0:
                    mPresenter.chooseGallery();
                    break;
                case 1:
                    mPresenter.chooseCamera();
                    break;
                case 2:
                    dialogInterface.cancel();
                    break;
            }
        });
        alertDialog.show();
    }

    @Override
    public String getUserName() {
        return mUserFullName.getText().toString();
    }

    @Override
    public String getUserPhone() {
        return mUserPhone.getText().toString();
    }

    @Override
    public boolean viewOnBackPressed() {
        if (mScreen.getCurrentState() == EDIT_STATE) {
            mPresenter.switchViewState();
            return true;
        } else {
            return false;
        }
    }

    public void updateProfileInfo(UserInfoDto userInfoDto) {
        mProfileName.setText(userInfoDto.getName());
        mUserFullName.setText(userInfoDto.getName());
        mUserPhone.setText(userInfoDto.getPhone());
        if (mScreen.getCurrentState() == PREVIEW_STATE) {
            mAvatarUri = Uri.parse(userInfoDto.getAvatar());
            insertAvatarPhoto();
        }
    }

    //endregion

    //region ==================== Events ====================

    @OnClick(R.id.collapsing_toolbar)
    void testEditMode() {
        mPresenter.switchViewState();
    }

    @OnClick(R.id.user_avatar_img)
    void clickChangeAvatar() {
        if (mScreen.getCurrentState() == EDIT_STATE) {
            mPresenter.takePhoto();
        }
    }

    @OnClick(R.id.addaddress_btn)
    void clickAddAddress() {
        mPresenter.clickOnAddress();
    }

    //endregion
}
