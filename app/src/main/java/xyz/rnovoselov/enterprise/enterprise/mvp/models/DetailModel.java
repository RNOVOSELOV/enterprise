package xyz.rnovoselov.enterprise.enterprise.mvp.models;

import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.CommentRealm;
import xyz.rnovoselov.enterprise.enterprise.jobs.SendMessagesJob;

/**
 * Created by roman on 28.01.17.
 */
public class DetailModel extends AbstractModel {

    public void sendComment(String id, CommentRealm commentRealm) {
        SendMessagesJob job = new SendMessagesJob(id, commentRealm);
        mJobManager.addJobInBackground(job);
    }
}
