package xyz.rnovoselov.enterprise.enterprise.mvp.models;

import com.birbit.android.jobqueue.JobManager;

import rx.Observable;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.data.network.req.UserLoginReq;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserRes;

/**
 * Created by roman on 22.10.16.
 */

public class AuthModel extends AbstractModel implements IAuthModel {
    public AuthModel() {

    }

    //for tests
    public AuthModel(DataManager dataManager, JobManager jobManager) {
        super(dataManager, jobManager);
    }

    @Override
    public boolean isUserAuth() {
        return mDataManger.isAuthUser();
    }

    @Override
    public Observable<UserRes> loginUser(String email, String password) {
        return mDataManger.loginUser(new UserLoginReq(email, password));
    }

    public void saveProfileInfo(UserRes userRes) {
        mDataManger.saveProfileInfo(userRes);
    }
}
