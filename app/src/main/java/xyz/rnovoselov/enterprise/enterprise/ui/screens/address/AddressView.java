package xyz.rnovoselov.enterprise.enterprise.ui.screens.address;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserAddressDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IAddressView;

/**
 * Created by roman on 08.12.16.
 */

public class AddressView extends RelativeLayout implements IAddressView {

    @Inject
    AddressScreen.AddressPresenter mPresenter;

    private int mAddressId = 0;

    @BindView(R.id.address_name_et)
    EditText addressNameEt;
    @BindView(R.id.address_street_et)
    EditText streetEt;
    @BindView(R.id.address_house_et)
    EditText houseEt;
    @BindView(R.id.address_appartment_et)
    EditText appartmentEt;
    @BindView(R.id.address_floor_et)
    EditText floorEt;
    @BindView(R.id.address_comment_et)
    EditText commentEt;
    @BindView(R.id.add_new_address_btn)
    Button addAddressBtn;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            ButterKnife.bind(this);
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
        Log.d("==== Callbacks", "onAttachedToWindow - AddressView");
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
        Log.d("==== Callbacks", "onDetachedFromWindow - AddressView");
    }

    //region ================ IAddressView ================

    public void initView(@Nullable UserAddressDto address) {
        if (address != null) {
            mAddressId = address.getId();
            addressNameEt.setText(address.getName());
            streetEt.setText(address.getStreet());
            houseEt.setText(address.getHouse());
            appartmentEt.setText(address.getApartment());
            floorEt.setText(String.valueOf(address.getFloor()));
            commentEt.setText(address.getComment());
            addAddressBtn.setText("Сохранить");
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public void showInputError() {
        showInputError("Ошибка добавления нового адреса");
    }

    @Override
    public void showInputError(String error) {
        Snackbar.make(this, error, Snackbar.LENGTH_LONG).show();
    }

    @Nullable
    @Override
    public UserAddressDto getUserAddress() {
        UserAddressDto address = null;
        String addressName = addressNameEt.getText().toString();
        String street = streetEt.getText().toString();
        String house = houseEt.getText().toString();
        String appartment = appartmentEt.getText().toString();
        int floor = 0;
        boolean floorIsUncorrect = false;
        try {
            floor = Integer.parseInt(floorEt.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: 11.12.16 send entered data to Fabric answers and crashlitics
            floorIsUncorrect = true;
        }
        String comment = commentEt.getText().toString();
        if (comment.length() <= 0) {
            comment = "комментарий отсутствует";
        }

        if (addressName.length() <= 0) {
            showInputError("Введите название места доставки");
        } else if (street.length() <= 0 || house.length() <= 0 || appartment.length() <= 0) {
            showInputError("Ошибка добавления адреса: улица, дом и номер квартиры не могут быть пустыми");
        } else if (floorIsUncorrect) {
            showInputError("Введен некорректный этаж");
        } else {
            address = new UserAddressDto(mAddressId,
                    addressName,
                    street,
                    house,
                    appartment,
                    floor,
                    comment);
        }
        return address;
    }

    //endregion

    //region ================ Events ================

    @OnClick(R.id.add_new_address_btn)
    void clickAddAddress() {
        mPresenter.clickOnAddAddress();
    }


    //endregion
}
