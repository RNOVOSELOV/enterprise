package xyz.rnovoselov.enterprise.enterprise.data.network.res;

import java.util.Date;

import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.CommentRealm;

/**
 * Created by roman on 06.02.17.
 */

public class CommentRes {
    private String id;
    private String avatar;
    private String userName;
    private float rating;
    private Date commentDate;
    private String comment;
    private boolean active;

    public CommentRes(String id, String avatar, String userName, int rating, Date date, String comment, boolean active) {
    }

    public CommentRes(String id, String avatar, String userName, float rating, Date commentDate, String comment, boolean active) {
        this.id = id;
        this.avatar = avatar;
        this.userName = userName;
        this.rating = rating;
        this.commentDate = commentDate;
        this.comment = comment;
        this.active = active;
    }

    public CommentRes(CommentRealm mComment) {
        this.avatar = mComment.getAvatar();
        this.userName = mComment.getUserName();
        this.rating = mComment.getRaiting();
        this.commentDate = mComment.getCommentDate();
        this.comment = mComment.getComment();
    }

    public String getId() {
        return id;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getUserName() {
        return userName;
    }

    public float getRating() {
        return rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }

    public boolean isActive() {
        return active;
    }
}
