package xyz.rnovoselov.enterprise.enterprise.mvp.views;

import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ProductDto;

/**
 * Created by roman on 20.11.16.
 */

public interface IProductView extends IView {
    void showProductView(ProductDto product);

    void updateProductCountView(ProductDto product);

    boolean isNormal();
}
