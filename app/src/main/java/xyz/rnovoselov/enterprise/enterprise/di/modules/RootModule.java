package xyz.rnovoselov.enterprise.enterprise.di.modules;

import dagger.Module;
import dagger.Provides;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.RootScope;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AccountModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;

/**
 * Created by roman on 03.12.16.
 */
@dagger.Module
public class RootModule {

    @Provides
    @RootScope
    public RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }

    @Provides
    @RootScope
    public AccountModel provideAccountModel () {
        return new AccountModel();
    }
}
