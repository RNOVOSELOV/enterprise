package xyz.rnovoselov.enterprise.enterprise.mvp.views;

import android.support.v4.view.ViewPager;

import java.util.List;

import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.MenuItemHolder;

/**
 * Created by roman on 26.01.17.
 */

public interface IActionBarView {
    void setTitle(CharSequence title);

    void setVisable(boolean visible);

    void setBackArrow(boolean enabled);

    void setMenuItem(List<MenuItemHolder> items);

    void setTabLayout(ViewPager pager);

    void removeTabLayout();
}
