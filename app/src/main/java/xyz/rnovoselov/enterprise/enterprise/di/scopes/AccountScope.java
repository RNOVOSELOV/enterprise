package xyz.rnovoselov.enterprise.enterprise.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by novoselov on 06.12.2016.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AccountScope {
}
