package xyz.rnovoselov.enterprise.enterprise.mvp.views;

import java.util.List;

import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ProductDto;

/**
 * Created by roman on 20.11.16.
 */

public interface ICatalogView extends IView {

    void showCatalogView();

    void updateProductCounter();
}
