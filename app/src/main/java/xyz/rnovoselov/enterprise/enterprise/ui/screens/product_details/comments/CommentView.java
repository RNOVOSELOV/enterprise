package xyz.rnovoselov.enterprise.enterprise.ui.screens.product_details.comments;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import io.realm.Realm;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.CommentRealm;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.AbstractView;

/**
 * Created by roman on 09.02.17.
 */
public class CommentView extends AbstractView<CommentScreen.CommentsPresenter>{

    private CommentsAdapter mAdapter = new CommentsAdapter();

    @BindView(R.id.comments_list)
    RecyclerView commentsList;

    public CommentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<CommentScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public CommentsAdapter getAdapter () {
        return mAdapter;
    }

    public void initView () {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        commentsList.setLayoutManager(linearLayoutManager);
        commentsList.setAdapter(mAdapter);
    }

    public void showAddCommentDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());

        View dialogView = inflater.inflate(R.layout.dialog_comment, null);

        AppCompatRatingBar ratingBar = ((AppCompatRatingBar) dialogView.findViewById(R.id.comment_rb));
        EditText commentEt = ((EditText) dialogView.findViewById(R.id.comment_et));

        dialogBuilder.setTitle("Оставить отзыв о товаре?")
                .setView(dialogView)
                .setPositiveButton("Оставить отзыв", (dialogInterface, i) -> {
                    CommentRealm commentRealm = new CommentRealm(ratingBar.getRating(), commentEt.getText().toString());
                    mPresenter.addComment(commentRealm);
                })
                .setNegativeButton("Отмена", ((dialogInterface, i) -> dialogInterface.cancel()))
                .show();
    }
}
