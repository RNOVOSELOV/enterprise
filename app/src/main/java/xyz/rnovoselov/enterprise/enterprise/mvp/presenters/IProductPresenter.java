package xyz.rnovoselov.enterprise.enterprise.mvp.presenters;

/**
 * Created by roman on 20.11.16.
 */

public interface IProductPresenter {

    void clickOnPlus();

    void clickOnMinus();
}
