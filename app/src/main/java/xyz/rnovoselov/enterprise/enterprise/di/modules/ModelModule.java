package xyz.rnovoselov.enterprise.enterprise.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;

/**
 * Created by roman on 27.11.16.
 */

@Module
public class ModelModule extends FlavorModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return DataManager.getInstance();
    }
}
