package xyz.rnovoselov.enterprise.enterprise.mvp.views;

/**
 * Created by roman on 04.12.16.
 */

public interface IAccountView extends IView {

    void changeState();

    void showEditState();

    void showPreviewState();

    void showPhotoSourceDialog();

    String getUserName();

    String getUserPhone();
}
