package xyz.rnovoselov.enterprise.enterprise.mvp.views;

import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserAddressDto;

/**
 * Created by roman on 08.12.16.
 */

public interface IAddressView extends IView {

    void showInputError();

    void showInputError(String error);

    UserAddressDto getUserAddress();
}
