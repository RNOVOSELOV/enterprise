package xyz.rnovoselov.enterprise.enterprise.ui.screens.account;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import xyz.rnovoselov.enterprise.enterprise.R;

/**
 * Created by novoselov on 13.12.2016.
 */

public abstract class ItemSwipeCallback extends ItemTouchHelper.SimpleCallback {

    private final int mLeftColor;
    private final int mRightColor;
    private final Drawable mLeftIcon;
    private final Drawable mRightIcon;
    private final String mLeftString;
    private final String mRightString;

    private final float mHorizontalTextMarginPx;
    private final float mTextOffsetY;
    private final float mTextSizePx;
    private final float mHorizontalIconMarginPx;
    private final float mIconSizePx;

    private final Paint mTextPaint;

    public ItemSwipeCallback(Context context, int dragDirs, int swipeDirs) {
        super(dragDirs, swipeDirs);

        // settings
        mLeftColor = ContextCompat.getColor(context, R.color.rv_edit_color);
        mRightColor = ContextCompat.getColor(context, R.color.rv_delete_color);
        mLeftIcon = ContextCompat.getDrawable(context, R.drawable.ic_mode_edit_white_24dp);
        mRightIcon = ContextCompat.getDrawable(context, R.drawable.ic_delete_white_24dp);
        mLeftString = context.getString(R.string.rv_edit_string).toUpperCase();
        mRightString = context.getString(R.string.rv_delete_string).toUpperCase();

        mHorizontalTextMarginPx = context.getResources().getDimensionPixelSize(R.dimen.margin_enlarge_56);
        mTextOffsetY = context.getResources().getDimensionPixelSize(R.dimen.rv_text_offset);
        mTextSizePx = context.getResources().getDimensionPixelSize(R.dimen.font_increased_16);
        mHorizontalIconMarginPx = context.getResources().getDimensionPixelSize(R.dimen.margin_small_16);
        mIconSizePx = context.getResources().getDimensionPixelSize(R.dimen.size_icons_24);

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextSize(mTextSizePx);
        mTextPaint.setAntiAlias(true);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public abstract void onSwiped(RecyclerView.ViewHolder viewHolder, int direction);

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            View itemView = viewHolder.itemView;
            float left = itemView.getLeft();
            float right = itemView.getRight();
            float top = itemView.getTop();
            float bottom = itemView.getBottom();
            float itemHeightPx = ((float) bottom) - ((float) top);

            if (dX > 0) {   // swipe to right
                RectF background = new RectF(left, top, dX, bottom);
                c.clipRect(background);
                c.drawColor(mLeftColor);

                int iconLeft = (int) (left + mHorizontalIconMarginPx);
                int iconTop = (int) (top + itemHeightPx / 2 - mIconSizePx / 2);
                int iconRight = (int) (left + mHorizontalIconMarginPx + mIconSizePx);
                int iconBottom = (int) (bottom - itemHeightPx / 2 + mIconSizePx / 2);
                mLeftIcon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                mLeftIcon.draw(c);

                mTextPaint.setTextAlign(Paint.Align.LEFT);
                float textX = (left + (mHorizontalTextMarginPx));
                float textY = (int) (top + itemHeightPx / 2 + mTextSizePx / 2 - mTextOffsetY);
                c.drawText(mLeftString, textX, textY, mTextPaint);
            } else {
                RectF background = new RectF(right + dX, top, right, bottom);
                c.clipRect(background);
                c.drawColor(mRightColor);

                mTextPaint.setTextAlign(Paint.Align.RIGHT);
                int textX = (int) (right - mHorizontalTextMarginPx);
                int textY = (int) (top + itemHeightPx / 2 + mTextSizePx / 2 - mTextOffsetY);
                c.drawText(mRightString, textX, textY, mTextPaint);

                int iconLeft = (int) (right - mHorizontalIconMarginPx - mIconSizePx);
                int iconTop = (int) (top + itemHeightPx / 2 - mIconSizePx / 2);
                int iconRight = (int) (right - mHorizontalIconMarginPx);
                int iconBottom = (int) (bottom - itemHeightPx / 2 + mIconSizePx / 2);
                mRightIcon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                mRightIcon.draw(c);

            }
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }
}
