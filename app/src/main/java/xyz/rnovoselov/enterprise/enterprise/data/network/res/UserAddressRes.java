package xyz.rnovoselov.enterprise.enterprise.data.network.res;

import com.squareup.moshi.Json;

/**
 * Created by roman on 18.03.17.
 */

public class UserAddressRes {
    @Json(name = "_id")
    private String id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private int floor;
    private String comment;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public int getFloor() {
        return floor;
    }

    public String getComment() {
        return comment;
    }
}