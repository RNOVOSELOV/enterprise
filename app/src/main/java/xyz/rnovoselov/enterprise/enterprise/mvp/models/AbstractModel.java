package xyz.rnovoselov.enterprise.enterprise.mvp.models;

import com.birbit.android.jobqueue.JobManager;

import javax.inject.Inject;

import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.components.DaggerModelComponent;
import xyz.rnovoselov.enterprise.enterprise.di.components.ModelComponent;
import xyz.rnovoselov.enterprise.enterprise.di.modules.ModelModule;

/**
 * Created by roman on 27.11.16.
 */

public abstract class AbstractModel {

    @Inject
    DataManager mDataManger;
    @Inject
    JobManager mJobManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    //for tests
    public AbstractModel(DataManager dataManager, JobManager jobManager) {
        this.mDataManger = dataManager;
        this.mJobManager = jobManager;
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
