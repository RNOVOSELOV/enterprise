package xyz.rnovoselov.enterprise.enterprise.di.components;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import xyz.rnovoselov.enterprise.enterprise.di.modules.PicassoCacheModule;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.RootScope;

/**
 * Created by roman on 27.11.16.
 */

@Component (dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
