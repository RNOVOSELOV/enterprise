package xyz.rnovoselov.enterprise.enterprise.ui.screens.product_details.comments;

import android.content.ContentResolver;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.CommentDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;

/**
 * Created by roman on 09.02.17.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {
    private static final String TAG = CommentsAdapter.class.getSimpleName();

    private List<CommentDto> mCommentsList = new ArrayList<>();

    @Inject
    Picasso mPicasso;

    public void addItem(CommentDto commentDto) {
        mCommentsList.add(commentDto);
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        DaggerService.<CommentScreen.Component>getDaggerComponent(recyclerView.getContext()).inject(this);
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        CommentDto commentDto = mCommentsList.get(position);
        holder.userNameTxt.setText(commentDto.getUserName());
        holder.dateTxt.setText(commentDto.getCommentDate());
        holder.ratingBar.setRating(commentDto.getRating());
        holder.commentTxt.setText(commentDto.getComment());

        String urlAvatar = commentDto.getAvatarUrl();
        if (urlAvatar == null || urlAvatar.isEmpty()) {
            urlAvatar = ContentResolver.SCHEME_ANDROID_RESOURCE +
                    "://" + holder.itemView.getContext().getResources().getResourcePackageName(R.drawable.avatar)
                    + '/' + holder.itemView.getContext().getResources().getResourceTypeName(R.drawable.avatar)
                    + '/' + holder.itemView.getContext().getResources().getResourceEntryName(R.drawable.avatar);
        }
        mPicasso.load(urlAvatar)
                .error(R.drawable.avatar)
                .fit()
                .into(holder.commentAvatarImg);
    }

    @Override
    public int getItemCount() {
        return mCommentsList.size();
    }

    public void reloadAdapter(List<CommentDto> commentDtos) {
        mCommentsList.clear();
        mCommentsList = commentDtos;
        notifyDataSetChanged();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.comment_avatar_img)
        ImageView commentAvatarImg;
        @BindView(R.id.user_name_txt)
        TextView userNameTxt;
        @BindView(R.id.date_txt)
        TextView dateTxt;
        @BindView(R.id.comment_txt)
        TextView commentTxt;
        @BindView(R.id.rating)
        AppCompatRatingBar ratingBar;

        public CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
