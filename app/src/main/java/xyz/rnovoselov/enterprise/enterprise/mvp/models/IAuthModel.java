package xyz.rnovoselov.enterprise.enterprise.mvp.models;

import rx.Observable;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserRes;

/**
 * Created by roman on 29.03.17.
 */

public interface IAuthModel {
    boolean isUserAuth();

    public Observable<UserRes> loginUser(String email, String password);
}
