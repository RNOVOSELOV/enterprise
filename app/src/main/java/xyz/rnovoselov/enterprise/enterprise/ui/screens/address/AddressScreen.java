package xyz.rnovoselov.enterprise.enterprise.ui.screens.address;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserAddressDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.AddressScope;
import xyz.rnovoselov.enterprise.enterprise.flow.AbstractScreen;
import xyz.rnovoselov.enterprise.enterprise.flow.Screen;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AccountModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.IAddressPresenter;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.account.AccountScreen;

/**
 * Created by roman on 08.12.16.
 */

@Screen(R.layout.screen_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {

    @Nullable
    private UserAddressDto mAddress;

    public AddressScreen(@Nullable UserAddressDto address) {
        mAddress = address;
    }

    @Override
    public boolean equals(Object o) {
        if (mAddress != null) {
            return (o instanceof AddressScreen) && (mAddress.equals(((AddressScreen) o).mAddress));
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        if (mAddress != null) {
            return mAddress.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region ================ DI ================

    @dagger.Module
    public class Module {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    public interface Component {
        void inject(AddressPresenter presenter);

        void inject(AddressView view);
    }

    //endregion

    //region ================ Presenter ================

    public class AddressPresenter extends ViewPresenter<AddressView> implements IAddressPresenter {

        @Inject
        AccountModel mAccountModel;

        @Inject
        RefWatcher mRefWatcher;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (mAddress != null && getView() != null) {
                getView().initView(mAddress);       // инициализируем только если есть адресс (иначе поля поставляем пустые)
            }
        }

        @Override
        protected void onExitScope() {
            mRefWatcher.watch(this);
            super.onExitScope();
        }

        @Override
        public void clickOnAddAddress() {
            if (getView() != null) {
                UserAddressDto address = getView().getUserAddress();
                if (address != null) {
                    mAccountModel.updateOrInsertAddress(address);
                    Flow.get(getView()).goBack();
                }
            }
        }
    }

    //endregion

}
