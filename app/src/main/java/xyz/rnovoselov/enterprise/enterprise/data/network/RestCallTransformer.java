package xyz.rnovoselov.enterprise.enterprise.data.network;

import android.support.annotation.VisibleForTesting;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import retrofit2.Response;
import rx.Observable;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.data.network.error.ErrorUtils;
import xyz.rnovoselov.enterprise.enterprise.data.network.error.ForbiddenApiError;
import xyz.rnovoselov.enterprise.enterprise.data.network.error.NetworkUnavailableError;
import xyz.rnovoselov.enterprise.enterprise.utils.ConstantManager;
import xyz.rnovoselov.enterprise.enterprise.utils.NetworkStatusChecker;

import static android.support.annotation.VisibleForTesting.NONE;

/**
 * Created by roman on 10.01.17.
 */

public class RestCallTransformer<R> implements Observable.Transformer<Response<R>, R> {
    private boolean mTestMode;

    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
        Observable<Boolean> networkStatus;
        if (mTestMode) {
            networkStatus = Observable.just(true);
        } else {
            networkStatus = NetworkStatusChecker.isInternetAvailable();
        }
        return networkStatus
                .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkUnavailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case 200:
                            String lastModified = rResponse.headers().get(ConstantManager.HEADER_LAST_MODIFIED);
                            if (lastModified != null) {
                                DataManager.getInstance().getPreferenceManager().saveLastProductUpdate(lastModified);
                            }
                            return Observable.just(rResponse.body());                   // данные с сервера
                        case 304:
                            return Observable.empty();                                  // на сервере нет изменений все брать с диска
                        case 403:
                            return Observable.error(new ForbiddenApiError());
                        default:
                            return Observable.error(ErrorUtils.parseError(rResponse));  // произошла какая либо ошибка
                    }
                });
    }

    @VisibleForTesting(otherwise = NONE)
    public void setTestMode() {
        mTestMode = true;
    }
}
