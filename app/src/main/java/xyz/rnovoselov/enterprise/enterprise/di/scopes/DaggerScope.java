package xyz.rnovoselov.enterprise.enterprise.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by roman on 26.01.17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface DaggerScope {
    Class<?> value();
}
