package xyz.rnovoselov.enterprise.enterprise.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import xyz.rnovoselov.enterprise.enterprise.BuildConfig;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserInfoDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.components.AppComponent;
import xyz.rnovoselov.enterprise.enterprise.di.modules.PicassoCacheModule;
import xyz.rnovoselov.enterprise.enterprise.di.modules.RootModule;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.RootScope;
import xyz.rnovoselov.enterprise.enterprise.flow.TreeKeyDispatcher;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AccountModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.MenuItemHolder;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IActionBarView;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IRootView;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IView;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.account.AccountScreen;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.catalog.CatalogScreen;


public class RootActivity extends AppCompatActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener, IActionBarView {

    protected ProgressDialog mProgressDialog;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinator;
    @BindView(R.id.navigation_view)
    NavigationView mNavigation;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;
    @BindView(R.id.appbar_layout)
    AppBarLayout mAppBar;
    @BindView(R.id.fab_btn)
    FloatingActionButton fab;

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    Picasso mPicasso;

    ActionBarDrawerToggle mToggle;
    private ActionBar mActionBar;
    private List<MenuItemHolder> mActionBarMenuItems;
    private static long lastBackPressedMills;

    public FloatingActionButton getFab() {
        return fab;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new CatalogScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        lastBackPressedMills = 0;
        ButterKnife.bind(this);
        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);

        initToolbar();
        mRootPresenter.takeView(this);
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView(this);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        mNavigation.setNavigationItemSelectedListener(this);
    }

    @Override
    public void initDrawer(UserInfoDto userInfoDto) {
        View header = mNavigation.getHeaderView(0);
        ImageView avatar = ((ImageView) header.findViewById(R.id.avatar_image));
        TextView userName = (TextView) header.findViewById(R.id.nv_name_user);

        userName.setText(userInfoDto.getName());

        mPicasso.load(userInfoDto.getAvatar())
                .error(R.drawable.avatar)
                .fit()
                .centerCrop()
                .into(avatar);
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            } else if (lastBackPressedMills + 2000 > System.currentTimeMillis()) {
                super.onBackPressed();
                ActivityCompat.finishAfterTransition(this);
            } else {
                lastBackPressedMills = System.currentTimeMillis();
                Snackbar.make(mCoordinator, "Вы действительно хотите выйти из приложения?", Snackbar.LENGTH_LONG)
                        .setAction("Да", view -> {
                            RootActivity.super.onBackPressed();
                            ActivityCompat.finishAfterTransition(RootActivity.this);
                        })
                        .show();
            }
        }
    }

    //region ==================== NAVIGATION LISTENER ====================
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Object key = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                key = new AccountScreen();
                break;
            case R.id.nav_catalog:
                key = new CatalogScreen();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_notifications:
                break;
            case R.id.nav_orders:
                break;
        }

        if (key != null) {
            Flow.get(this).set(key);
        }

        item.setChecked(true);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
    //endregion

    //region =========================== IRootView ===========================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinator, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable error) {
        if (BuildConfig.DEBUG) {
            showMessage(error.getMessage());
            error.printStackTrace();
        } else {
            showMessage("Извините, что то пошло не так, попробуйте позже");
            //TODO send error statistics to crashlytics
        }
    }

    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.dialog_progress);
        } else {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.dialog_progress);
        }
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mRootPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRootPresenter.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    public boolean isAllGranted(@NonNull String[] permissions, boolean allGranted) {
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission(this, permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        return allGranted;
    }

    //endregion

    //region ================ ActionBar ================

    @Override
    public void setVisable(boolean visible) {
        // TODO: 26.01.17 implement this
    }

    @Override
    public void setBackArrow(boolean enabled) {
        if (mToggle != null && mActionBar != null) {
            if (enabled) {
                mToggle.setDrawerIndicatorEnabled(false);                   // скрываем индикатор toggle
                mActionBar.setDisplayHomeAsUpEnabled(true);                 // устанавливаем индикатор туллбара
                if (mToggle.getToolbarNavigationClickListener() == null) {
                    mToggle.setToolbarNavigationClickListener(view -> onBackPressed()); // вешаем обработчик
                }
            } else {
                mActionBar.setDisplayHomeAsUpEnabled(false);                            // скрываем индикатор туллбара
                mToggle.setDrawerIndicatorEnabled(true);                                // активируем индикатор toggle
                mToggle.setToolbarNavigationClickListener(null);                        // зануляем обработчик
            }

            //  если есть возможность вернуться назад (стрелка назад в ActionBar), то блокируем раскрытие NavigationDrawer
            mDrawerLayout.setDrawerLockMode(enabled ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
            mToggle.syncState();            // синхронизируем состояние toggle c NavigationDrawer
        }
    }

    @Override
    public void setMenuItem(List<MenuItemHolder> items) {
        mActionBarMenuItems = items;
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mActionBarMenuItems != null && !mActionBarMenuItems.isEmpty()) {
            for (MenuItemHolder menuItemHolder : mActionBarMenuItems) {
                MenuItem item = menu.add(menuItemHolder.getTitle());
                item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                        .setIcon(menuItemHolder.getIconResId())
                        .setOnMenuItemClickListener(menuItemHolder.getListener());
            }
        } else {
            menu.clear();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTabLayout(ViewPager pager) {
        TabLayout tabView = new TabLayout(this);        // создаем tabLayout
        tabView.setupWithViewPager(pager);              // связываем его с ViewPager
        mAppBar.addView(tabView);                       // добавляем табы в аппбар

        // регистрируем обработчик переключения по табам для вью пейджера
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
    }

    @Override
    public void removeTabLayout() {
        View tabView = mAppBar.getChildAt(1);
        if (tabView != null && tabView instanceof TabLayout) {  // проверяем есть ли у аппбара дочерняя вью - инстанс таблэйаута
            mAppBar.removeView(tabView);                        // удаляем ее
        }
    }

    //endregion

    //region ================================== DI ==================================

    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {
        void inject(RootActivity activity);

        void inject(SplashActivity activity);

        void inject(RootPresenter presenter);

        AccountModel getAccountModel();

        RootPresenter getRootPresenter();

        Picasso getPicasso();

        RefWatcher getRefWatcher();
    }

    //endregion
}
