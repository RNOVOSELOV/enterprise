package xyz.rnovoselov.enterprise.enterprise.di.components;

import javax.inject.Singleton;

import dagger.Component;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.di.modules.LocalModule;
import xyz.rnovoselov.enterprise.enterprise.di.modules.NetworkModule;

/**
 * Created by roman on 27.11.16.
 */

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);

}
