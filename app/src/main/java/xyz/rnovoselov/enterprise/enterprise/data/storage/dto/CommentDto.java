package xyz.rnovoselov.enterprise.enterprise.data.storage.dto;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.SimpleDateFormat;

import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.CommentRealm;

/**
 * Created by roman on 10.02.17.
 */
public class CommentDto {

    private String userName;
    private String avatarUrl;
    private float rating;
    private String commentDate;
    private String comment;

    public CommentDto(CommentRealm commentRealm) {

        //SimpleDateFormat df = new SimpleDateFormat("dd MMMM yy");
        PrettyTime pt = new PrettyTime();
        this.userName = commentRealm.getUserName();
        this.avatarUrl = commentRealm.getAvatar();
        this.rating = commentRealm.getRaiting();
        //this.commentDate = df.format(commentRealm.getCommentDate());
        this.commentDate = pt.format(commentRealm.getCommentDate());
        this.comment = commentRealm.getComment();
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public float getRating() {
        return rating;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }
}
