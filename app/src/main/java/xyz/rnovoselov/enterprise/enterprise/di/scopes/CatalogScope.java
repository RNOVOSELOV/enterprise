package xyz.rnovoselov.enterprise.enterprise.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by roman on 27.11.16.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CatalogScope {
}
