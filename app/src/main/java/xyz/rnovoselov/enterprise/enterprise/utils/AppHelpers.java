package xyz.rnovoselov.enterprise.enterprise.utils;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;

/**
 * Created by roman on 06.12.16.
 */

public class AppHelpers {

    /**
     * Метод подсчитвает на сколько сжался элемент в долях относитоельно максимально возможной высоты
     *
     * @param start        минимальный размер
     * @param end          максимальный размер
     * @param currentValue текущий размер
     * @return текущий размер элемента в диапазоне 0 ... 1, где 0 - минимальный размер; 1 - максимальный
     */
    public static float currentFriction(int start, int end, int currentValue) {
        return (float) (currentValue - start) / (end - start);
    }

    /**
     * Метод возвращает размер элемента на основании параметра сжатия
     *
     * @param start    минимальная высота
     * @param end      максимальная высота
     * @param friction текущее местоположение в диапазоне 0 ... 1
     * @return положение между положениями start и end на основании параметра friction
     */
    public static int sizeFromFriction(int start, int end, float friction) {
        return (int) (start + (end - start) * friction);
    }

    public static Activity getActivityFromViewContext(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }
}
