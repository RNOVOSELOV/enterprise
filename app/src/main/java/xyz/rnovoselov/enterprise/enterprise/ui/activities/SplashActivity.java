package xyz.rnovoselov.enterprise.enterprise.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import xyz.rnovoselov.enterprise.enterprise.BuildConfig;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserInfoDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.flow.TreeKeyDispatcher;
import xyz.rnovoselov.enterprise.enterprise.mortar.ScreenScoper;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IRootView;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IView;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.auth.AuthScreen;
import xyz.rnovoselov.enterprise.enterprise.utils.ConstantManager;

public class SplashActivity extends AppCompatActivity implements IRootView {

    private static final String TAG = ConstantManager.TAG_PREFIX + RootActivity.class.getSimpleName();
    private static final int PROGRESS_ACTIVE = 1;

    protected ProgressDialog mProgressDialog;

    @BindView(R.id.login_coordinator)
    CoordinatorLayout mCoordinator;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    @Inject
    RootPresenter mRootPresenter;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(@NonNull String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    //region ==================== LIFE CYCLE ====================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        DaggerService.<RootActivity.RootComponent>getDaggerComponent(this).inject(this);

        /*
        mAuthPanel.initInputFields(mPresenter);
        */

        if (savedInstanceState != null) {
            if (savedInstanceState.getInt(ConstantManager.PROGRESS_DIALOG_SHOWN, 0) == PROGRESS_ACTIVE) {
                showLoad();
            }
        }
    }

    @Override
    protected void onStart() {
        mRootPresenter.takeView(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mRootPresenter.dropView(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
        }
        super.onDestroy();
    }

    //endregion

    //region ==================== IAuthView ====================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinator, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable error) {
        if (BuildConfig.DEBUG) {
            showMessage(error.getMessage());
            error.printStackTrace();
        } else {
            showMessage("Извините, что то пошло не так, попробуйте позже");
            //TODO send error statistics to crashlytics
        }
    }

    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.dialog_progress);
        } else {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.dialog_progress);
        }
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

    @Override
    public void initDrawer(UserInfoDto userInfoDto) {

    }

    //endregion


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            outState.putInt(ConstantManager.PROGRESS_DIALOG_SHOWN, PROGRESS_ACTIVE);
        } else {
            outState.putInt(ConstantManager.PROGRESS_DIALOG_SHOWN, 0);
        }
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public void startRootActivity() {
        Intent intent = new Intent(this, RootActivity.class);
        startActivity(intent);
        ActivityCompat.finishAfterTransition(this);
    }

    /*
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                mPresenter.onClickLogin();
                break;
            case R.id.login_show_catalog_btn:
                mPresenter.onClickShowCatalog();
                break;
            case R.id.login_fb_btn:
                startSocialClickAnimation(view);
                mPresenter.onClickFb();
                break;
            case R.id.login_tw_btn:
                startSocialClickAnimation(view);
                mPresenter.onClickTwitter();
                break;
            case R.id.login_vk_btn:
                startSocialClickAnimation(view);
                mPresenter.onClickVk();
                break;
        }
    }
    */
}
