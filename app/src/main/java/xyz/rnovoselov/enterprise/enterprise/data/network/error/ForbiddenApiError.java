package xyz.rnovoselov.enterprise.enterprise.data.network.error;

/**
 * Created by roman on 03.04.17.
 */

public class ForbiddenApiError extends ApiError {
    public ForbiddenApiError() {
        super("Неверный логин или пароль");
    }
}
