package xyz.rnovoselov.enterprise.enterprise.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

import xyz.rnovoselov.enterprise.enterprise.data.network.res.ProductRes;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;

/**
 * Created by roman on 20.11.16.
 */
public class ProductDto implements Parcelable {
    private int id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private int count;
    private boolean favorite;

    public ProductDto(ProductRes productRes) {
        this(productRes, new ProductLocalInfo());
    }

    public ProductDto(ProductRes productRes, ProductLocalInfo productLocalInfo) {
        this.id = productRes.getRemoteId();
        this.productName = productRes.getProductName();
        this.imageUrl = productRes.getImageUrl();
        this.description = productRes.getDescription();
        this.price = productRes.getPrice();
        this.count = productLocalInfo.getCount();
        this.favorite = productLocalInfo.isFavorite();
    }

    public ProductDto(int id, String productName, String imageUrl, String description, int price, int count) {
        this.id = id;
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.count = count;
    }

    public ProductDto(ProductRealm mProduct) {
        this.productName = mProduct.getProductName();
        this.imageUrl = mProduct.getImageUrl();
        this.description = mProduct.getDescription();
        this.price = mProduct.getPrice();
        this.count = mProduct.getCount();
        this.favorite = mProduct.isFavorite();
    }

    //region ============================ PARCELABLE ============================

    protected ProductDto(Parcel in) {
        id = in.readInt();
        productName = in.readString();
        imageUrl = in.readString();
        description = in.readString();
        price = in.readInt();
        count = in.readInt();
    }

    public static final Creator<ProductDto> CREATOR = new Creator<ProductDto>() {
        @Override
        public ProductDto createFromParcel(Parcel in) {
            return new ProductDto(in);
        }

        @Override
        public ProductDto[] newArray(int size) {
            return new ProductDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(productName);
        parcel.writeString(imageUrl);
        parcel.writeString(description);
        parcel.writeInt(price);
        parcel.writeInt(count);
    }

    //endregion

    //region ============================= GETTERS ==============================

    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public boolean isFavorite() {
        return favorite;
    }
//endregion

    public void deleteProduct() {
        count--;
    }

    public void addProduct () {
        count++;
    }

    public void updateData(ProductRes productRes) {
        this.id = productRes.getRemoteId();
        this.productName = productRes.getProductName();
        this.imageUrl = productRes.getImageUrl();
        this.description = productRes.getDescription();
        this.price = productRes.getPrice();
    }

    public void updateData(ProductLocalInfo productLocalInfo) {
        this.count = productLocalInfo.getCount();
        this.favorite = productLocalInfo.isFavorite();
    }
}
