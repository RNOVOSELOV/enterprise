package xyz.rnovoselov.enterprise.enterprise.mvp.presenters;

import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserAddressDto;

/**
 * Created by roman on 04.12.16.
 */

public interface IAccountPresenter {

    void clickOnAddress();

    void editAddress(int position);

    void removeAddress(int position);

    void switchViewState();

    void takePhoto();

    void chooseCamera();

    void chooseGallery();
}
