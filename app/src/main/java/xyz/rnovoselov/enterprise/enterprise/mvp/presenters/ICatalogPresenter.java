package xyz.rnovoselov.enterprise.enterprise.mvp.presenters;

/**
 * Created by roman on 20.11.16.
 */

public interface ICatalogPresenter {

    void clickOnBuyButton(int position);

    boolean checkUserAuth();
}
