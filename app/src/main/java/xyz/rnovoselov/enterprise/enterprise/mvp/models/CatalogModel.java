package xyz.rnovoselov.enterprise.enterprise.mvp.models;

import android.support.annotation.Nullable;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.List;

import rx.Observable;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ProductDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;

/**
 * Created by roman on 20.11.16.
 */

public class CatalogModel extends AbstractModel {

    public CatalogModel() {

    }

    public void addBasketCounter () {
        mDataManger.getPreferenceManager().saveBasketCounter(mDataManger.getPreferenceManager().getBacketCounter() + 1);
    }

    public boolean isUserAuth() {
        return mDataManger.isAuthUser();
    }

    public Observable<ProductRealm> getProductObs() {
        Observable<ProductRealm> disk = fromDisk();
        Observable<ProductRealm> network = fromNetwork();
        return Observable.mergeDelayError(disk, network)
                .distinct(ProductRealm::getId);

//        Observable<ProductLocalInfo> local = network.flatMap(productRes -> mDataManger.getProductLocalInfoObs(productRes));
//        Observable<ProductDto> productsFromNetwork = Observable.zip(network, local, ProductDto::new);
//        return Observable.merge(disk, productsFromNetwork)  // склеиваем два потока данных с диска и из сети
//                .onErrorResumeNext(throwable -> ((throwable instanceof NetworkUnavailableError) || (throwable instanceof ApiError))
//                        ? disk : Observable.error(throwable)) // вернуть запись с диска, если сетевая ошибка произошла тут может быть проверка на тип ошибки
//                .distinct(ProductDto::getId);               // чтобы не было дубликатов товаров
    }

    @RxLogObservable
    public Observable<ProductRealm> fromNetwork() {
        return mDataManger.getProductObsFromNetwork();
    }

    @RxLogObservable
    public Observable<ProductRealm> fromDisk() {
        return mDataManger.getProductFromRealm();
    }
}
