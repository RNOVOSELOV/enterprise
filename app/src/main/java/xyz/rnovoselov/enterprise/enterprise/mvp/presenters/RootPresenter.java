package xyz.rnovoselov.enterprise.enterprise.mvp.presenters;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mortar.MortarScope;
import mortar.Presenter;
import mortar.bundler.BundleService;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ActivityResultDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserInfoDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AccountModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IRootView;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.RootActivity;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.SplashActivity;

/**
 * Created by roman on 28.11.16.
 */

public class RootPresenter extends Presenter<IRootView> {

    private static int DEFAULT_MODE = 0;
    private static int TAB_MODE = 1;

    private PublishSubject<ActivityResultDto> mActivityResultDtoObs = PublishSubject.create();

    @Inject
    AccountModel mAccountModel;

    private Subscription userInfoSub;

    private String mPhotoFileUri;
//    private BehaviorSubject<ActivityResultDto> mActiviryResultSubject = BehaviorSubject.create();
//    private BehaviorSubject<ActivityPermissionResultDto> mActivityPermissionResultSubject = BehaviorSubject.create();

    public RootPresenter() {
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        ((RootActivity.RootComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected BundleService extractBundleService(IRootView view) {
        return (view instanceof RootActivity) ?
                BundleService.getBundleService(((RootActivity) view)) :     // привязываем RootPresenter к RootActivity
                BundleService.getBundleService(((SplashActivity) view));    // или SplashActivity
    }

    public FloatingActionButton getFab() {
        return ((RootActivity) getRootView()).getFab();
    }

    public void hideFab() {
        if (getFab().hasOnClickListeners()) {
            getFab().setOnClickListener(null);
        }
        getFab().hide();
    }

    public void showFab() {
        getFab().show();
    }

    public void setOnClickListenerOnFab(View.OnClickListener listener) {
        getFab().setOnClickListener(listener);
    }

    public PublishSubject<ActivityResultDto> getActivityResultDtoObs() {
        return mActivityResultDtoObs;
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        if (getView() instanceof RootActivity) {
            userInfoSub = subscribeOnUserInfoObs();
        }
    }

    @Override
    public void dropView(IRootView view) {
        if (userInfoSub != null) {
            userInfoSub.unsubscribe();
        }
        super.dropView(view);
    }

    private Subscription subscribeOnUserInfoObs() {
        return mAccountModel.getUserInfoObs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new UserInfoSubscriber());
    }

    @Nullable
    public IRootView getRootView() {
        return getView();
    }

    // для теста необходимо дернуть реальный метод
    public ActionBarBuilder newActionBarBuilder() {
        return this.new ActionBarBuilder();
    }

    @RxLogSubscriber
    private class UserInfoSubscriber extends Subscriber<UserInfoDto> {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if (getView() != null) {
                getView().showError(e);
            }
        }

        @Override
        public void onNext(UserInfoDto userInfoDto) {
            if (getView() != null) {
                getView().initDrawer(userInfoDto);
            }
        }
    }


    public boolean checkPermissionsAndRequestIfNotGranted(@NonNull String[] permissions, int requestCode) {
        boolean allGranted = true;
        if (getView() == null) {
            return false;
        }
        allGranted = ((RootActivity) getView()).isAllGranted(permissions, allGranted);

        if (!allGranted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ((RootActivity) getView()).requestPermissions(permissions, requestCode);
            }
            return false;
        }
        return allGranted;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        mActivityResultDtoObs.onNext(new ActivityResultDto(requestCode, resultCode, intent));
    }

    public void onRequestPermissionResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // TODO: 26.01.17 implement this
    }

    public class ActionBarBuilder {
        private boolean isGoBack = false;
        private boolean isVisible = true;
        private CharSequence title;
        private List<MenuItemHolder> items = new ArrayList<>();
        private ViewPager pager;
        private int toolbarMode = DEFAULT_MODE;

        public ActionBarBuilder setBackArrow(boolean enable) {
            this.isGoBack = enable;
            return this;
        }

        public ActionBarBuilder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public ActionBarBuilder setVisible(boolean visible) {
            this.isVisible = visible;
            return this;
        }

        public ActionBarBuilder addAction(MenuItemHolder menuItem) {
            this.items.add(menuItem);
            return this;
        }

        public ActionBarBuilder setTab(ViewPager pager) {
            this.toolbarMode = TAB_MODE;
            this.pager = pager;
            return this;
        }

        public void build() {
            if (getView() != null) {
                RootActivity activity = (RootActivity) getView();
                activity.setVisable(isVisible);
                activity.setTitle(title);
                activity.setBackArrow(isGoBack);
                activity.setMenuItem(items);
                if (toolbarMode == TAB_MODE) {
                    activity.setTabLayout(pager);
                } else {
                    activity.removeTabLayout();
                }
            }
        }
    }
}
