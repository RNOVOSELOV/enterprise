package xyz.rnovoselov.enterprise.enterprise.ui.screens.product_details.description;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.AttributeSet;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.DescriptionDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.DaggerScope;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.AbstractView;

/**
 * Created by roman on 10.02.17.
 */
public class DescriptionView extends AbstractView<DescriptionScreen.DescriptionPresenter> {

    @BindView(R.id.full_description_txt)
    TextView fullDescriptionTxt;
    @BindView(R.id.product_count_tv)
    TextView productCountTxt;
    @BindView(R.id.product_price_tv)
    TextView productPriceTxt;
    @BindView(R.id.product_rating)
    AppCompatRatingBar productRating;

    public DescriptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DescriptionScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public void initView(DescriptionDto descriptionDto) {
        fullDescriptionTxt.setText(descriptionDto.getDescription());
        productRating.setRating(descriptionDto.getRating());
        productCountTxt.setText(String.valueOf(descriptionDto.getCount()));

        if (descriptionDto.getCount() > 0) {
            productPriceTxt.setText(String.format("%d %s", descriptionDto.getCount() * descriptionDto.getPrice(), ".-"));
        } else {
            productPriceTxt.setText(String.valueOf(descriptionDto.getPrice() + ".-"));
        }
    }

    //region ================ Events ================

    @OnClick(R.id.plus_btn)
    void clickOnPlus() {
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.minus_btn)
    void clickOnMinus () {
        mPresenter.clickOnMinus();
    }

    //endregion
}
