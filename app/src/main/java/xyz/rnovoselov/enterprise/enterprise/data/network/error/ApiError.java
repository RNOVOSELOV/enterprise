package xyz.rnovoselov.enterprise.enterprise.data.network.error;

/**
 * Created by roman on 10.01.17.
 */

public class ApiError extends Throwable {

    private int statusCode;
    private String message;

    public ApiError() {
        super("Неизвестная ошибка сервера");
    }

    public ApiError(int statusCode) {
        super("Ошибка сервера " + statusCode);
        this.statusCode = statusCode;
    }

    public ApiError(String s) {
        super(s);
    }
}
