package xyz.rnovoselov.enterprise.enterprise.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import xyz.rnovoselov.enterprise.enterprise.data.managers.PreferencesManager;

/**
 * Created by roman on 27.11.16.
 */

@Module
public class LocalModule extends FlavorLocalModule {

    @Provides
    @Singleton
    PreferencesManager providePreferenceManager(Context context) {
        return new PreferencesManager(context);
    }
}
