package xyz.rnovoselov.enterprise.enterprise.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import io.realm.Realm;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.CommentRes;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.CommentRealm;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;
import xyz.rnovoselov.enterprise.enterprise.utils.AppConfig;

/**
 * Created by roman on 21.02.17.
 */

public class SendMessagesJob extends Job {

    private final static String TAG = SendMessagesJob.class.getSimpleName();
    private final CommentRealm mComment;
    private final String mProductId;

    public SendMessagesJob(String productId, CommentRealm comment) {
        super(new Params(JobPriority.MID)       // Приоритет задачи
                .requireNetwork()               // Задаче для выполнения необходимо соединение с сетью
                .persist()                      // Задача персистентна (должна выполниться вне зависимости от состония сети)
                .groupBy("Comments"));          // Группа задач - задачи в группе выполняются поочередно (возможно в разных потоках)
        mComment = comment;
        mProductId = productId;
    }

    @Override
    public void onAdded() {
        // Задача была добавлена
        Log.d(TAG, "SendMessageJob: onAdded");
        Realm realm = Realm.getDefaultInstance();
        ProductRealm product = realm.where(ProductRealm.class)
                .equalTo("id", mProductId)
                .findFirst();

        realm.executeTransaction(realm1 -> product.getCommentRealms().add(mComment));   // Добавляем комментарий в локальную БД
        realm.close();
    }

    @Override
    public void onRun() throws Throwable {
        // Задача начала выполняться
        Log.d(TAG, "SendMessageJob: onRun");

        CommentRes commentRes = new CommentRes(mComment);
        DataManager.getInstance().sendComment(mProductId, commentRes)               // Отправляем комментарий на сервер
                .subscribe(commentRes1 -> {                                         // при ошибке ответа onRun испустит Throwable
                    Realm realm = Realm.getDefaultInstance();                       // и автоматически вызовется shouldReRunOnThrowable
                    CommentRealm localComment = realm.where(CommentRealm.class)
                            .equalTo("id", mComment.getId())
                            .findFirst();
                    ProductRealm product = realm.where(ProductRealm.class)
                            .equalTo("id", mProductId)
                            .findFirst();
                    CommentRealm serverComment = new CommentRealm(commentRes1);
                    realm.executeTransaction(realm1 -> {
                        localComment.deleteFromRealm();                 // Удаляем комментарий из локальной базы (т.к. заперщено перезаписывать PRIMARY KEY)
                        product.getCommentRealms().add(serverComment);  // Добавляем коментарий с сервера в базу
                    });
                    realm.close();
                });
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        // Задача отменена/завершена
        Log.d(TAG, "SendMessageJob: onCancel");

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        // Ошибка выполнения задачи, политика повторений разрешается здесь
        Log.d(TAG, "SendMessageJob: shouldReRunOnThrowable runcount: " + runCount + " maxCount: " + maxRunCount);
        return RetryConstraint.createExponentialBackoff(runCount, AppConfig.INITIAL_BACK_OFF_IN_MS);
        //return RetryConstraint.RETRY;             // Поочередное выполнение задач без задержки, количетсов повторов maxRunCount
    }
}
