package xyz.rnovoselov.enterprise.enterprise.ui.screens.account;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.squareup.leakcanary.RefWatcher;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Observable;
import rx.Subscription;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ActivityResultDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserAddressDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserInfoDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserSettingsDto;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.AccountScope;
import xyz.rnovoselov.enterprise.enterprise.flow.AbstractScreen;
import xyz.rnovoselov.enterprise.enterprise.flow.Screen;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.AccountModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.IAccountPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.RootPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.SubscribePresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IRootView;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.RootActivity;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.address.AddressScreen;
import xyz.rnovoselov.enterprise.enterprise.utils.ConstantManager;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by roman on 04.12.16.
 */

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCurrentState = 1;

    public int getCurrentState() {
        return mCurrentState;
    }

    public void setCurrentState(int mCurrentState) {
        this.mCurrentState = mCurrentState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .module(new Module())
                .rootComponent(parentComponent)
                .build();
    }

    //region ================ DI ================

    @dagger.Module
    public class Module {

        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }

    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter presenter);

        void inject(AccountView view);

        RootPresenter getRootPresenter();

        AccountModel getAccountModel();

        RefWatcher getRefWatcher();
    }

    //endregion

    //region ================ Presenter ================

    public class AccountPresenter extends SubscribePresenter<AccountView> implements IAccountPresenter {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        AccountModel mAccountModel;
        @Inject
        RefWatcher mRefWatcher;

        private Subscription mAddressSub;
        private Subscription mSettingsSub;

        private Uri mAvatarUri;
        private File mPhotoFile;
        private Subscription mActivityResultSub;
        private Subscription mUserInfoSub;

        //region ================ LifeCycle ================

        @Override
        protected void onEnterScope(MortarScope scope) {        // Презентер создается
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            subscribeOnActivityResult();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {      // презентер загружает и отображает данные во вью
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView();
            }
            subscribeOnAddressObs();
            subscribeOnSettingsObs();
            subscribeOnUserInfoObs();
        }

        @Override
        protected void onSave(Bundle outState) {                // когда вью отвязывается от виндоу
            super.onSave(outState);
            mAddressSub.unsubscribe();
            mSettingsSub.unsubscribe();
            mUserInfoSub.unsubscribe();
        }

        @Override
        protected void onExitScope() {                          // когда выходим из области видимости
            mRefWatcher.watch(this);
            mActivityResultSub.unsubscribe();
            super.onExitScope();
        }

        //endregion

        //region ================ Subscription ================

        private void subscribeOnAddressObs() {

            mAddressSub = subscribe(mAccountModel.getAddressObs(), new ViewSubscriber<UserAddressDto>() {
                @Override
                public void onNext(UserAddressDto addressDto) {
                    if (getView() != null) {
                        getView().getAdapter().addItem(addressDto);
                    }
                }
            });

        }

        private void updateListView() {
            if (getView() != null) {
                getView().getAdapter().reloadAdapter();
                subscribeOnAddressObs();
            }
        }

        private void subscribeOnSettingsObs() {
            mSettingsSub = subscribe(mAccountModel.getUserSettingsObs(), new ViewSubscriber<UserSettingsDto>() {
                @Override
                public void onNext(UserSettingsDto userSettingsDto) {
                    if (getView() != null) {
                        getView().initSettings(userSettingsDto);
                    }
                }
            });
        }

        private void subscribeOnActivityResult() {
            Observable<ActivityResultDto> activityResultObs = mRootPresenter.getActivityResultDtoObs()
                    .filter(activityResultDto -> activityResultDto.getResultCode() == Activity.RESULT_OK);

            mActivityResultSub = subscribe(activityResultObs, new ViewSubscriber<ActivityResultDto>() {
                @Override
                public void onNext(ActivityResultDto activityResultDto) {
                    handleActivityResult(activityResultDto);
                }
            });
        }

        private void handleActivityResult(ActivityResultDto activityResultDto) {
            // TODO: 23.12.16 migrate to RX
            switch (activityResultDto.getRequestCode()) {
                case ConstantManager.REQUEST_PROFILE_PHOTO_PICKER:
                    if (activityResultDto.getIntent() != null) {
                        String photoUrl = activityResultDto.getIntent().getData().toString();
                        getView().updateAvatarPhoto(Uri.parse(photoUrl));
                    }
                    break;
                case ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA:
                    if (mPhotoFile != null) {
                        getView().updateAvatarPhoto(Uri.fromFile(mPhotoFile));
                    }
                    break;
                default:
                    Log.d("=====", String.valueOf(activityResultDto.getRequestCode()) + " " + String.valueOf(activityResultDto.getResultCode()));
            }
        }

        private void subscribeOnUserInfoObs () {
            mUserInfoSub = subscribe(mAccountModel.getUserInfoObs(), new ViewSubscriber<UserInfoDto>() {
                @Override
                public void onNext(UserInfoDto userInfoDto) {
                    if (getView() != null) {
                        getView().updateProfileInfo(userInfoDto);
                    }
                }
            });
        }

        //endregion

        @Override
        public void clickOnAddress() {
            Flow.get(getView()).set(new AddressScreen(null));
        }

        @Override
        public void editAddress(int position) {
            Flow.get(getView()).set(new AddressScreen(mAccountModel.getAddresFromPosition(position)));
        }

        @Override
        public void removeAddress(int position) {
            mAccountModel.removeAddress(mAccountModel.getAddresFromPosition(position));
            updateListView();
        }

        @Override
        public void switchViewState() {
            if (getCurrentState() == AccountView.EDIT_STATE && getView() != null) {
                mAccountModel.saveProfileInfo(getView().getUserProfileInfo());
            }
            if (getView() != null) {
                getView().changeState();
            }
        }

        @Override
        public void takePhoto() {
            if (getView() != null) {
                getView().showPhotoSourceDialog();
            }
        }

        //region ================ Camera ================

        @Override
        public void chooseCamera() {
            if (getRootView() != null) {
                String[] permissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};
                if (mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions, ConstantManager.REQUEST_PERMISSION_CAMERA)) {
                    mPhotoFile = createFileForPhoto();
                    if (mPhotoFile == null) {
                        getRootView().showMessage("Фотография не может быть создана");
                        return;
                    }
                    takePhotoFromCamera();
                }
            }
        }

        private File createFileForPhoto() {
            DateFormat dateTimeInstance = SimpleDateFormat.getTimeInstance(DateFormat.MEDIUM);
            String timeStamp = dateTimeInstance.format(new Date());
            String imageFileName = "IMG_" + timeStamp.replace(" ", "").replace(":", "_");
            File storageDir = getView().getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File fileImg;
            try {
                fileImg = File.createTempFile(imageFileName, ".jpg", storageDir);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return fileImg;
        }

        private void takePhotoFromCamera() {
            Uri uriForFile = FileProvider.getUriForFile(((RootActivity) getRootView()),
                    ConstantManager.FILE_PROVIDER_AUTHORITY,
                    mPhotoFile);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriForFile);
            ((RootActivity) getRootView()).startActivityForResult(takePictureIntent, ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA);
        }

        //endregion

        //region ================ Gallery ===============

        @Override
        public void chooseGallery() {
            if (getRootView() != null) {
                String[] permissions = new String[]{READ_EXTERNAL_STORAGE};
                if (mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions, ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)) {
                    takePhotoFromGallery();
                }
            }
        }

        private void takePhotoFromGallery() {
            Intent intent = new Intent();
            if (Build.VERSION.SDK_INT < 19) {
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
            } else {
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }
            ((RootActivity) getRootView()).startActivityForResult(intent, ConstantManager.REQUEST_PROFILE_PHOTO_PICKER);
        }

        //endregion

        @Nullable
        @Override
        protected IRootView getRootView() {
            return mRootPresenter.getRootView();
        }

        public List<UserAddressDto> getAddresses() {
            return mAccountModel.getUserAddresses();
        }

        public void switchSettings() {
            if (getView() != null) {
                mAccountModel.saveSettings(getView().getSettings());
            }
        }
    }

    //endregion

}
