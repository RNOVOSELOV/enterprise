package xyz.rnovoselov.enterprise.enterprise.ui.screens.product;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.ChangeImageTransform;
import com.transitionseverywhere.Explode;
import com.transitionseverywhere.SidePropagation;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ProductDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ProductLocalInfo;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.AbstractView;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.IProductView;
import xyz.rnovoselov.enterprise.enterprise.ui.custom_views.AspectRatioImageView;
import xyz.rnovoselov.enterprise.enterprise.utils.ViewHelper;

/**
 * Created by roman on 04.12.16.
 */

public class ProductView extends AbstractView<ProductScreen.ProductPresenter> implements IProductView {

    private static final String TAG = ProductView.class.getSimpleName();

    public static final int STATE_NORMAL = 0;
    public static final int STATE_SHOW_IMAGE = 1;

    @BindView(R.id.product_name_txt)
    TextView productNameTxt;
    @BindView(R.id.product_description_txt)
    TextView productDescriptionTxt;
    @BindView(R.id.product_image)
    AspectRatioImageView productImageIv;
    @BindView(R.id.product_count_txt)
    TextView productCountTxt;
    @BindView(R.id.product_price_txt)
    TextView productPriceTxt;
    @BindView(R.id.favorite_btn)
    CheckBox favoriteBtn;
    @BindView(R.id.product_wrapper)
    ViewGroup productWrapper;
    @BindView(R.id.product_vav_wrap)
    LinearLayout productVavWrapper;
    @BindView(R.id.product_card)
    CardView productCard;

    @Inject
    Picasso mPicasso;
    private AnimatorSet mResultSet;
    private ArrayList<View> mChildList;
    private boolean isZoomed;
    private int mInImageHeight;
    private int mInImageWidth;
    private float mDen;

    public ProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<ProductScreen.Component>getDaggerComponent(context).inject(this);
        mDen = ViewHelper.getDensity(context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            showProductViewFromState();
        }
    }

    private void showProductViewFromState() {
        if (!isNormal()) {
            isZoomed = true;
            showImageState();
        } else {
            isZoomed = false;
            showNormalState();
        }
    }

    //region ============================ ProductView ============================

    @Override
    public void showProductView(final ProductDto product) {
        productNameTxt.setText(product.getProductName());
        productDescriptionTxt.setText(product.getDescription());
        updateProductCountView(product);

        favoriteBtn.setChecked(product.isFavorite());

        mPicasso.load(product.getImageUrl())
                .placeholder(R.drawable.no_photo)
                .networkPolicy(NetworkPolicy.OFFLINE)
//                .fit()
//                .centerInside()
                .into(productImageIv, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.e(TAG, "Picasso onSuccess: load from cache");
                    }

                    @Override
                    public void onError() {
                        mPicasso.load(product.getImageUrl())
                                .placeholder(R.drawable.no_photo)
//                                .fit()
//                                .centerInside()
                                .into(productImageIv);
                    }
                });
    }

    public ProductLocalInfo getProductLocalInfo() {
        return new ProductLocalInfo(0, favoriteBtn.isChecked(), Integer.parseInt(productCountTxt.getText().toString()));
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        productCountTxt.setText(String.valueOf(product.getCount()));
        productPriceTxt.setText(String.valueOf(product.getPrice()) + ".-");
        if (product.getCount() > 0) {
            productPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice()) + ".-");
        }
    }

    @Override
    public boolean isNormal() {
        return mPresenter.getProductCardState() == STATE_NORMAL;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion

    //region ================ Events ================

    @OnClick(R.id.plus_btn)
    void onClickPlus() {
        mPresenter.clickOnPlus();
    }

    @OnClick(R.id.minus_btn)
    void onClickMinus() {
        mPresenter.clickOnMinus();
    }

    @OnClick(R.id.favorite_btn)
    void clickOnFavorite() {
        mPresenter.clickFavorite();
    }

    @OnClick(R.id.show_more_btn)
    void clickOnShowMore() {
        mPresenter.clickShowMore();
    }

    @OnClick(R.id.product_image)
    void clickOnImage() {
        startZoomTransition();
    }

    //endregion

    //region ================ Animation ================

    public void startAddToCardAnim() {
        final int cx = (productWrapper.getLeft() + productWrapper.getRight()) / 2;  // Центр карточки по Х
        final int cy = (productWrapper.getTop() + productWrapper.getBottom()) / 2;  // Центр карточки по Y
        final int radius = Math.max(productWrapper.getWidth(), productWrapper.getHeight()); // Вычисляем радиус

        final Animator hideCircleAnim;
        final Animator showCircleAnim;
        Animator hideColorAnim = null;
        Animator showColorAnim = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Создаем анимацию для объекта карточки из центра с максимальным радиусом до нуля
            // Анимация Reveal всегд адолжна пересоздаваться!!!! нельзя переиспользовать
            hideCircleAnim = ViewAnimationUtils.createCircularReveal(productWrapper, cx, cy, radius, 0);
            hideCircleAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    productWrapper.setVisibility(INVISIBLE);
                }
            });

            showCircleAnim = ViewAnimationUtils.createCircularReveal(productWrapper, cx, cy, 0, radius);
            showCircleAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationEnd(animation);
                    productWrapper.setVisibility(VISIBLE);
                }
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ColorDrawable cdr = ((ColorDrawable) productWrapper.getForeground());
                hideColorAnim = ObjectAnimator.ofArgb(cdr, "color", ContextCompat.getColor(getContext(), R.color.color_accent));
                showColorAnim = ObjectAnimator.ofArgb(cdr, "color", ContextCompat.getColor(getContext(), R.color.color_transparent));
            }

        } else {
            // TODO: 04.03.17 add any animation for old version
            hideCircleAnim = ObjectAnimator.ofFloat(productWrapper, "alpha", 0);
            showCircleAnim = ObjectAnimator.ofFloat(productWrapper, "alpha", 1);
        }

        AnimatorSet hideSet = new AnimatorSet();
        AnimatorSet showSet = new AnimatorSet();

        addAnimatorTogetherInSet(hideSet, hideCircleAnim, hideColorAnim);   // Добавляем аниматоры в сет
        addAnimatorTogetherInSet(showSet, showCircleAnim, showColorAnim);

        hideSet.setDuration(600);                                           // Длительность анимации
        hideSet.setInterpolator(new FastOutSlowInInterpolator());           // Устанавливаем временную функцию

        showSet.setStartDelay(300);
        showSet.setDuration(600);                                           // Задержка анимации появления
        showSet.setInterpolator(new FastOutSlowInInterpolator());

        if ((mResultSet != null && !mResultSet.isStarted()) || mResultSet == null) {
            mResultSet = new AnimatorSet();
            mResultSet.playSequentially(hideSet, showSet);                       // результирующий сет последовательно проигрывает два сета
            mResultSet.start();
        }

        Log.e(TAG, "mResultSet start: " + mResultSet.isStarted());
        Log.e(TAG, "mResultSet running: " + mResultSet.isRunning());

//        showCircleAnim.setStartDelay(1000);
//        hideSet.playSequentially(hideCircleAnim, showCircleAnim);
//        hideSet.start();
    }

    private void addAnimatorTogetherInSet(AnimatorSet set, Animator... anims) {
        ArrayList<Animator> animatorList = new ArrayList<>();
        for (Animator animator : anims) {
            if (animator != null) {
                animatorList.add(animator);
            }
        }
        set.playTogether(animatorList);
    }

    private void startZoomTransition() {

        TransitionSet set = new TransitionSet();

        Transition explode = new Explode();         // Анимация исчезновения от эпицентра (почти как Slide, но от заданного эпицентра)
        final Rect rect = new Rect(productImageIv.getLeft(), productImageIv.getTop(), productImageIv.getRight(), productImageIv.getBottom());
        explode.setEpicenterCallback(new Transition.EpicenterCallback() {   // установка эпицентра (эпицентр - изображение товара)
            @Override
            public Rect onGetEpicenter(Transition transition) {
                return rect;
            }
        });
        SidePropagation prop = new SidePropagation();       // чем удаленнее отьет от эпицентра, тем позже начнется его анимация
        prop.setPropagationSpeed(3f);       // по умолчанию 3 - скорость движения удаленных объектов относительно
        // первого движущегося обьекта (по сути относительная задержка)

        explode.setPropagation(prop);

        ChangeBounds changeBounds = new ChangeBounds();
        ChangeImageTransform imageTransform = new ChangeImageTransform();

        if (!isZoomed) {
            changeBounds.setStartDelay(100);
            imageTransform.setStartDelay(100);
        }

        set.addTransition(explode)
                .addTransition(changeBounds)
                .addTransition(imageTransform)
                .setDuration(600)
                .setInterpolator(new FastOutSlowInInterpolator());

        TransitionManager.beginDelayedTransition(productCard, set);


        if (!isZoomed) {
            showImageState();
        } else {
            showNormalState();
        }

        isZoomed = !isZoomed;
    }

    private void showNormalState() {
        if (mChildList == null) {
            mChildList = ViewHelper.getChildsExcludeView(productWrapper, R.id.product_image);
        }

        ViewGroup.LayoutParams cardParams = productCard.getLayoutParams();
        cardParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        productCard.setLayoutParams(cardParams);

        ViewGroup.LayoutParams wrapParam = productWrapper.getLayoutParams();
        wrapParam.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        productWrapper.setLayoutParams(wrapParam);

        ViewGroup.LayoutParams imgParams;
        if (getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgParams = new RelativeLayout.LayoutParams(mInImageWidth, ViewGroup.LayoutParams.MATCH_PARENT);
            int defMargin = (int) (8 * mDen);
            ((RelativeLayout.LayoutParams) imgParams).setMargins(0, 0, defMargin, 0);
            ((RelativeLayout.LayoutParams) imgParams).addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        } else {
            imgParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mInImageHeight);
            int defMargin = (int) (16 * mDen);
            ((LinearLayout.LayoutParams) imgParams).setMargins(defMargin, 0, defMargin, 0);
        }
        productImageIv.setScaleType(ImageView.ScaleType.CENTER_CROP);
        productImageIv.enableAspectRatio(true);
        productImageIv.setLayoutParams(imgParams);

        for (View view : mChildList) {
            view.setVisibility(VISIBLE);
        }
        mPresenter.setProductCardState(STATE_NORMAL);
    }

    private void showImageState() {
        if (mChildList == null) {
            mChildList = ViewHelper.getChildsExcludeView(productWrapper, R.id.product_image);
        }

        ViewGroup.LayoutParams cardParams = productCard.getLayoutParams();
        cardParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
        productCard.setLayoutParams(cardParams);

        ViewGroup.LayoutParams wrapParam = productWrapper.getLayoutParams();
        wrapParam.height = ViewGroup.LayoutParams.MATCH_PARENT;
        productWrapper.setLayoutParams(wrapParam);

        ViewGroup.LayoutParams imgParams;
        mInImageHeight = productImageIv.getHeight();
        mInImageWidth = productImageIv.getWidth();
        if (getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        } else {
            imgParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        productImageIv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        productImageIv.enableAspectRatio(false);
        productImageIv.setLayoutParams(imgParams);

        for (View view : mChildList) {
            view.setVisibility(GONE);
        }
        mPresenter.setProductCardState(STATE_SHOW_IMAGE);
    }

    //endregion
}
