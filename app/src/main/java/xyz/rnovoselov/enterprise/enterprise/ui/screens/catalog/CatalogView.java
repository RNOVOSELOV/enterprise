package xyz.rnovoselov.enterprise.enterprise.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;

import butterknife.BindView;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.AbstractView;
import xyz.rnovoselov.enterprise.enterprise.mvp.views.ICatalogView;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.product.ProductView;

/**
 * Created by roman on 04.12.16.
 */

public class CatalogView extends AbstractView<CatalogScreen.CatalogPresenter> implements ICatalogView {

    @BindView(R.id.add_to_card_btn)
    Button addToCardButton;
    @BindView(R.id.product_pager)
    ViewPager productPager;
    @BindView(R.id.pager_indicator)
    CircleIndicator mIndicator;
    private CatalogAdapter mAdapter;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        mAdapter = new CatalogAdapter();
    }

    @Override
    public void showCatalogView() {
        productPager.setAdapter(mAdapter);
        mIndicator.setViewPager(productPager);
        mAdapter.registerDataSetObserver(mIndicator.getDataSetObserver());
    }

    @Override
    public void updateProductCounter() {
        // TODO: 20.11.16 update count products on card icon
    }

    public int getCurrentPagerPosition() {
        return productPager.getCurrentItem();
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @OnClick(R.id.add_to_card_btn)
    void clickAddToCard() {
        mPresenter.clickOnBuyButton(productPager.getCurrentItem());
    }

    public CatalogAdapter getAdapter() {
        return mAdapter;
    }

    public ProductView getCurrentProductView() {
        return (ProductView) productPager.findViewWithTag("Product" + productPager.getCurrentItem());
    }
}
