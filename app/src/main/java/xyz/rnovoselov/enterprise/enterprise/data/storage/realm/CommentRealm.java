package xyz.rnovoselov.enterprise.enterprise.data.storage.realm;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import xyz.rnovoselov.enterprise.enterprise.data.managers.DataManager;
import xyz.rnovoselov.enterprise.enterprise.data.managers.PreferencesManager;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.CommentRes;

/**
 * Created by roman on 06.02.17.
 */

public class CommentRealm extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String userName;
    private String avatar;
    private float raiting;
    private Date commentDate;
    private String comment;

    // Необходимо для Realm
    public CommentRealm() {
    }

    public CommentRealm(float raiting, String comment) {
        this.id = String.valueOf(this.hashCode());

        final PreferencesManager pm = DataManager.getInstance().getPreferenceManager();
        this.userName = pm.getUserFullName();
        this.avatar = pm.getUserAvatar();
        this.commentDate = new Date();
        this.raiting = raiting;
        this.comment = comment;
    }

    public CommentRealm(CommentRes commentRes) {
        this.id = commentRes.getId();
        this.userName = commentRes.getUserName();
        this.avatar = commentRes.getAvatar();
        this.raiting = commentRes.getRating();
        this.commentDate = commentRes.getCommentDate();
        this.comment = commentRes.getComment();
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public float getRaiting() {
        return raiting;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }
}
