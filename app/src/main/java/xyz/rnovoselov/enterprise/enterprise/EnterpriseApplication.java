package xyz.rnovoselov.enterprise.enterprise;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import io.realm.Realm;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.components.AppComponent;
import xyz.rnovoselov.enterprise.enterprise.di.components.DaggerAppComponent;
import xyz.rnovoselov.enterprise.enterprise.di.modules.AppModule;
import xyz.rnovoselov.enterprise.enterprise.di.modules.PicassoCacheModule;
import xyz.rnovoselov.enterprise.enterprise.di.modules.RootModule;
import xyz.rnovoselov.enterprise.enterprise.mortar.ScreenScoper;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.DaggerRootActivity_RootComponent;
import xyz.rnovoselov.enterprise.enterprise.ui.activities.RootActivity;

/**
 * Created by roman on 22.10.16.
 */

public class EnterpriseApplication extends Application {

    private static AppComponent sAppComponent;
    private static RootActivity.RootComponent mRootActivityRootComponent;

    private MortarScope mRootScope;

    private RefWatcher refWatcher;
    private MortarScope mRootActivityScope;

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static RootActivity.RootComponent getRootActivityComponent() {
        return mRootActivityRootComponent;
    }

    @Override
    public Object getSystemService(String name) {
        if (mRootScope != null) {
            return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
        } else {
            return super.getSystemService(name);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();


        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        refWatcher = LeakCanary.install(this);

        Realm.init(this);

        createAppComponent();
        createRootActivityComponent();

        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");
        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private void createAppComponent() {
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext(), refWatcher))
                .build();
    }

    private void createRootActivityComponent() {
        mRootActivityRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(sAppComponent)
                .rootModule(new RootModule())
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }
}

