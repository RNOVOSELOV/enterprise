package xyz.rnovoselov.enterprise.enterprise.data.network.res;

/**
 * Created by roman on 22.02.17.
 */
public class AvatarUrlRes {
    private String avatarUrl;

    public AvatarUrlRes(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
