package xyz.rnovoselov.enterprise.enterprise.mvp.views;

/**
 * Created by roman on 20.11.16.
 */

public interface IView {

    boolean viewOnBackPressed();
}
