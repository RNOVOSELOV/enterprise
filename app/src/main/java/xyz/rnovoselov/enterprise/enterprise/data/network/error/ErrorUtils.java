package xyz.rnovoselov.enterprise.enterprise.data.network.error;

import retrofit2.Response;

/**
 * Created by roman on 10.01.17.
 */

public class ErrorUtils {

    public static ApiError parseError(Response<?> response) {
        ApiError error;

/*        try {
            error = ((ApiError) DataManager.getInstance()
                    .getRetrofit().responseBodyConverter(ApiError.class, ApiError.class.getAnnotations())
                    .convert(response.errorBody()));
        } catch (IOException e) {
            e.printStackTrace();
            return new ApiError();
        }*/
        error = new ApiError(response.code());
        // TODO: 03.04.17 Correct parse error (withiut retrofit dependency)

        return error;
    }
}
