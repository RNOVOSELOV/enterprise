package xyz.rnovoselov.enterprise.enterprise.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mortar.MortarScope;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;

/**
 * Created by roman on 20.11.16.
 */

public class CatalogAdapter extends PagerAdapter {

    private static final String TAG = CatalogAdapter.class.getSimpleName();

    private List<ProductRealm> mProductList = new ArrayList<>();

    public CatalogAdapter() {
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public void addItem(ProductRealm product) {
        mProductList.add(product);
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ProductRealm product = mProductList.get(position);
        Context productContext = CatalogScreen.Factory.createProductContext(product, container.getContext());
        View newView = LayoutInflater.from(productContext).inflate(R.layout.screen_product, container, false);
        newView.setTag("Product" + position);   // Добавляем тэк к вью продукта
        container.addView(newView);
        return newView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        MortarScope screenScope = MortarScope.getScope(((View) object).getContext());
        container.removeView(((View) object));
        screenScope.destroy();
        Log.e(TAG, "DestroyItem with name: " + screenScope.getName());
    }
}
