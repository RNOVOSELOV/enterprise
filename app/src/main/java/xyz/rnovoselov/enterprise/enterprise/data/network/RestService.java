package xyz.rnovoselov.enterprise.enterprise.data.network;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;
import xyz.rnovoselov.enterprise.enterprise.data.network.req.UserLoginReq;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.AvatarUrlRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.CommentRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.ProductRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserRes;
import xyz.rnovoselov.enterprise.enterprise.utils.ConstantManager;

/**
 * Created by roman on 27.11.16.
 */

public interface RestService {
    @GET ("products")
    Observable<Response<List<ProductRes>>> getProductResObs(@Header(ConstantManager.HEADER_IF_MODIFIED_SINCE) String lastEntityUpdate);

    @POST("products/{productId}/comments")
    Observable<CommentRes> sendComment(@Path("productId") String productId, @Body CommentRes commentRes);

    @Multipart
    @POST("avatar")
    Observable<AvatarUrlRes> uploadUserAvatar(@Part MultipartBody.Part file);

    @POST("login")
    Observable<Response<UserRes>> loginUser(@Body UserLoginReq userLoginReq);
}
