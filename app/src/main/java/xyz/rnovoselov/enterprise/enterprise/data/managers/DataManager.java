package xyz.rnovoselov.enterprise.enterprise.data.managers;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import xyz.rnovoselov.enterprise.enterprise.EnterpriseApplication;
import xyz.rnovoselov.enterprise.enterprise.data.network.RestCallTransformer;
import xyz.rnovoselov.enterprise.enterprise.data.network.RestService;
import xyz.rnovoselov.enterprise.enterprise.data.network.req.UserLoginReq;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.AvatarUrlRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.CommentRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.ProductRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserAddressRes;
import xyz.rnovoselov.enterprise.enterprise.data.network.res.UserRes;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ProductLocalInfo;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserAddressDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserSettingsDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.components.DaggerDataManagerComponent;
import xyz.rnovoselov.enterprise.enterprise.di.components.DataManagerComponent;
import xyz.rnovoselov.enterprise.enterprise.di.modules.LocalModule;
import xyz.rnovoselov.enterprise.enterprise.di.modules.NetworkModule;
import xyz.rnovoselov.enterprise.enterprise.utils.AppConfig;
import xyz.rnovoselov.enterprise.enterprise.utils.ConstantManager;
import xyz.rnovoselov.enterprise.enterprise.utils.NetworkStatusChecker;

import static android.support.annotation.VisibleForTesting.NONE;

/**
 * Created by roman on 22.10.16.
 */

public class DataManager {

    private static final String TAG = ConstantManager.TAG_PREFIX + DataManager.class.getSimpleName();

    private static DataManager INSTANCE = null;
    private final RestCallTransformer mRestCallTransformer;

    @Inject
    PreferencesManager mPreferenceManager;
    @Inject
    RestService mRestService;
    @Inject
    Context mApplicationContext;
    @Inject
    Retrofit mRetrofit;
    @Inject
    RealmManager mRealmManager;

    private ArrayList<UserAddressDto> mUserAddressesList;

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    private DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if (component == null) {
            component = DaggerDataManagerComponent.builder()
                    .appComponent(EnterpriseApplication.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);
        mRestCallTransformer = new RestCallTransformer<>();
//        generateMockData();

        updateLocalDataWithTimer(); // // TODO: 24.02.17 переделать
    }

    @VisibleForTesting(otherwise = NONE)
    DataManager(RestService restService) {
        this.mRestService = restService;
        mRestCallTransformer = new RestCallTransformer<>();
        mRestCallTransformer.setTestMode();
        INSTANCE = this;
    }

    @VisibleForTesting(otherwise = NONE)
    DataManager(PreferencesManager preferencesManager) {
        mPreferenceManager = preferencesManager;
        mRestCallTransformer = new RestCallTransformer<>();
        mRestCallTransformer.setTestMode();
        INSTANCE = this;
    }

    @VisibleForTesting(otherwise = NONE)
    DataManager(RestService mRestService, PreferencesManager mPreferenceManager, RealmManager mRealmManager) {
        this.mPreferenceManager = mPreferenceManager;
        this.mRestService = mRestService;
        this.mRealmManager = mRealmManager;
        mRestCallTransformer = new RestCallTransformer<>();
        mRestCallTransformer.setTestMode();
        INSTANCE = this;
    }

    private void updateLocalDataWithTimer() {
        Log.e(TAG, "LOCAL UPDATE start " + new Date());
        Observable.interval(AppConfig.UPDATE_DATA_UNTERVAL, TimeUnit.SECONDS)   // генерируем послед каждые 30 секунд
                .flatMap(aLong -> NetworkStatusChecker.isInternetAvailable())   // проверяем доступность сети
                .filter(aBoolean -> aBoolean)                                   // если сеть доступна идем дальше
                .flatMap(aBoolean -> getProductObsFromNetwork())                // запрашиваем данные
                .subscribe(productRealm -> {
                    Log.e(TAG, "LOCAL UPDATE complete: ");
                }, throwable -> {
                    throwable.printStackTrace();
                    Log.e(TAG, "LOCAL UPDATE error: " + throwable.getMessage());
                });
    }

    public Context getContext() {
        return mApplicationContext;
    }

    public PreferencesManager getPreferenceManager() {
        return mPreferenceManager;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public Observable<ProductRealm> getProductObsFromNetwork() {
        return mRestService.getProductResObs(mPreferenceManager.getLastProductUpdate())
                .compose(((RestCallTransformer<List<ProductRes>>) mRestCallTransformer))   // трансформируем response выбрасываем ApiError в случае ошибки,
                // проверяем статус сети перед запросом,
                // обрабатываем коды ответа
                .flatMap(Observable::from)                          // преобразуем список товаров в последовательность
                .subscribeOn(Schedulers.newThread())
//                .observeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(productRes -> {
                    if (!productRes.isActive()) {
                        mRealmManager.deleteFromRealm(ProductRealm.class, productRes.getId());      // удалить из БД
                    }
                })
                .filter(ProductRes::isActive)                                       // далее идут только активные товары
                .doOnNext(productRes -> mRealmManager.saveProductResponceToRealm(productRes))                  // сохраняем только активные товары
                .retryWhen(errorObservable ->
                        errorObservable
                                // генерируем последовательность чисел от 1 до 5 (колличество повторений запроса)
                                .zipWith(Observable.range(1, AppConfig.RETRY_REQUEST_COUNT), (throwable, retryCount) -> retryCount)
                                .doOnNext(retryCount -> Log.e(TAG, "LOCAL UPDATE request retry count: " + retryCount + " " + new Date()))
                                // рассчитываем экспотенциальную задержку
                                .map(retryCount -> ((long) (AppConfig.RETRY_REQUEST_BASE_DELAY * Math.pow(Math.E, retryCount))))
                                .doOnNext(delay -> Log.e(TAG, "LOCAL UPDATE delay: " + delay))
                                // создаем и возвращаем задержку в миллисекундах
                                .flatMap(delay -> Observable.timer(delay, TimeUnit.MILLISECONDS))
                )
                .flatMap(productRes -> Observable.empty());
    }

    public Observable<ProductRealm> getProductFromRealm() {
        return mRealmManager.getAllProductsFromRealm();
    }

    public Observable<ProductLocalInfo> getProductLocalInfoObs(ProductRes productRes) {
        return Observable.just(getPreferenceManager().getLocalInfo(productRes.getRemoteId()))
                .flatMap(productLocalInfo ->
                        productLocalInfo == null ?
                                Observable.just(new ProductLocalInfo()) :
                                Observable.just(productLocalInfo));
    }

    public boolean isAuthUser() {
        return getToken() != null && !getToken().isEmpty();
    }

    private void saveToken(String token) {
        getPreferenceManager().saveAuthToken(token);
    }

    private String getToken() {
        return getPreferenceManager().getAuthToken();
    }

    public Map<String, String> getUserProfileInfo() {
        Map<String, String> userProfileInfo = new HashMap<>();
        userProfileInfo.put(PreferencesManager.PROFILE_FULL_MANE_KEY, mPreferenceManager.getUserFullName());
        userProfileInfo.put(PreferencesManager.PROFILE_AVATAR_KEY, mPreferenceManager.getUserAvatar(mApplicationContext));
        userProfileInfo.put(PreferencesManager.PROFILE_PHONE_KEY, mPreferenceManager.getUserPhone());
        return userProfileInfo;
    }

    public void saveUserProfileInfo(String name, String phone, String ava) {
        mPreferenceManager.saveUserFullName(name);
        mPreferenceManager.saveUserPhone(phone);
        saveUserAvatar(ava);
    }

    public void saveUserAvatar(String avatarPath) {
        mPreferenceManager.saveUserAvatar(avatarPath);
    }

    public void addAddress(UserAddressDto userAddressDto) {
        userAddressDto.setId(mUserAddressesList.size());
        mUserAddressesList.add(userAddressDto);
    }

    public ArrayList<UserAddressDto> getUserAddresses() {
        return mUserAddressesList;
    }

    public void removeAddress(UserAddressDto address) {
        mUserAddressesList.remove(address);
    }

    public Map<String, Boolean> getUserSettings() {
        Map<String, Boolean> userSettings = new HashMap<>();
        userSettings.put(PreferencesManager.NOTIFICATION_ORDER_KEY, mPreferenceManager.getOrderNotificationParameter());
        userSettings.put(PreferencesManager.NOTIFICATION_PROMO_KEY, mPreferenceManager.getPromoNotificationParameter());
        return userSettings;
    }

    public void saveSettings(UserSettingsDto settings) {
        mPreferenceManager.saveNotificationParameter(PreferencesManager.NOTIFICATION_ORDER_KEY, settings.isOrderNotification());
        mPreferenceManager.saveNotificationParameter(PreferencesManager.NOTIFICATION_PROMO_KEY, settings.isPromoNotification());
    }

    public void updateOrInsertAddress(UserAddressDto addressDto) {
        int newAddressId = 0;
        for (UserAddressDto address : mUserAddressesList) {
            if (addressDto.getId() == address.getId()) {
                address.updateData(addressDto);
                return;
            }
            if (address.getId() >= newAddressId) {
                newAddressId = address.getId() + 1;
            }
        }
        // Добавление нового адреса
        addressDto.setId(newAddressId);
        addAddress(addressDto);
    }

    public Observable<AvatarUrlRes> uploadUserPhoto(MultipartBody.Part body) {
        return mRestService.uploadUserAvatar(body);
    }

    public Observable<CommentRes> sendComment(String productId, CommentRes comment) {
        return mRestService.sendComment(productId, comment);
    }

    public Observable<UserRes> loginUser(UserLoginReq userLoginReq) {
        return mRestService.loginUser(userLoginReq)
                .compose((RestCallTransformer<UserRes>) mRestCallTransformer)
                .doOnNext(userRes -> saveProfileInfo(userRes));

        /*return mRestService.loginUser(userLoginReq).flatMap(userResResponse -> {
            switch (userResResponse.code()) {
                case 200:
                    return Observable.just(userResResponse.body());
                case 403:
                    return Observable.error(new AccessError());
                default:
                    return Observable.error(new ApiError(userResResponse.code()));
            }
        });*/
    }

    public void saveCartProductCounter(int count) {
        mPreferenceManager.saveBasketCounter(count);
    }

    public int loadCartProductCounter() {
        return mPreferenceManager.getBacketCounter();
    }


    public void saveProfileInfo(UserRes userRes) {
        mPreferenceManager.saveProfileInfo(userRes);
        mUserAddressesList = new ArrayList<>();

        for (UserAddressRes address : userRes.getAddresses()) {
            addAddress(new UserAddressDto(mUserAddressesList.size(), address));
        }
    }

}
