package xyz.rnovoselov.enterprise.enterprise.mvp.models;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.ArrayList;
import java.util.Map;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import xyz.rnovoselov.enterprise.enterprise.data.managers.PreferencesManager;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserAddressDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserInfoDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.UserSettingsDto;
import xyz.rnovoselov.enterprise.enterprise.jobs.UploadAvatarJob;

/**
 * Created by roman on 04.12.16.
 */

public class AccountModel extends AbstractModel {

    private BehaviorSubject<UserInfoDto> mUserInfoObs = BehaviorSubject.create();

    public AccountModel() {
        mUserInfoObs.onNext(getUserProfileInfo());
    }

    //region ================ Address ================

    public ArrayList<UserAddressDto> getUserAddresses() {
        return mDataManger.getUserAddresses();
    }

    public void updateOrInsertAddress(UserAddressDto addressDto) {
        mDataManger.updateOrInsertAddress(addressDto);
    }

    public void removeAddress(UserAddressDto addressDto) {
        mDataManger.removeAddress(addressDto);
    }

    public UserAddressDto getAddresFromPosition(int position) {
        return getUserAddresses().get(position);
    }

    public Observable<UserAddressDto> getAddressObs() {
        return Observable.from(getUserAddresses());
    }

    //endregion

    //region ================ Settings ===============

    private UserSettingsDto getUserSettings() {
        Map<String, Boolean> map = mDataManger.getUserSettings();
        return new UserSettingsDto(map.get(PreferencesManager.NOTIFICATION_ORDER_KEY), map.get(PreferencesManager.NOTIFICATION_PROMO_KEY));
    }

    public Observable<UserSettingsDto> getUserSettingsObs() {
        return Observable.just(getUserSettings());
    }

    public void saveSettings(UserSettingsDto settings) {
        mDataManger.saveSettings(settings);
    }

    //endregion

    //region ================== User =================

    public void saveProfileInfo(UserInfoDto userProfileInfo) {
        mDataManger.saveUserProfileInfo(userProfileInfo.getName(),
                userProfileInfo.getPhone(),
                userProfileInfo.getAvatar());
        mUserInfoObs.onNext(userProfileInfo);

        String uriAvatar = userProfileInfo.getAvatar();
        if (!uriAvatar.contains("http")) {
            uploadAvatarOnServer(uriAvatar);
        }

    }

    private UserInfoDto getUserProfileInfo() {
        Map<String, String> map = mDataManger.getUserProfileInfo();
        return new UserInfoDto(map.get(PreferencesManager.PROFILE_FULL_MANE_KEY),
                map.get(PreferencesManager.PROFILE_PHONE_KEY),
                map.get(PreferencesManager.PROFILE_AVATAR_KEY));
    }

    @RxLogObservable
    public Observable<UserInfoDto> getUserInfoObs () {
        return mUserInfoObs;
    }

    private void uploadAvatarOnServer(String imageUri) {
        mJobManager.addJobInBackground(new UploadAvatarJob(imageUri));
    }

    //endregion
}
