package xyz.rnovoselov.enterprise.enterprise.ui.screens.product;

import android.os.Bundle;

import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import mortar.MortarScope;
import xyz.rnovoselov.enterprise.enterprise.R;
import xyz.rnovoselov.enterprise.enterprise.data.storage.dto.ProductDto;
import xyz.rnovoselov.enterprise.enterprise.data.storage.realm.ProductRealm;
import xyz.rnovoselov.enterprise.enterprise.di.DaggerService;
import xyz.rnovoselov.enterprise.enterprise.di.scopes.ProductScope;
import xyz.rnovoselov.enterprise.enterprise.flow.AbstractScreen;
import xyz.rnovoselov.enterprise.enterprise.flow.Screen;
import xyz.rnovoselov.enterprise.enterprise.mvp.models.CatalogModel;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.enterprise.enterprise.mvp.presenters.IProductPresenter;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.catalog.CatalogScreen;
import xyz.rnovoselov.enterprise.enterprise.ui.screens.product_details.DetailScreen;

/**
 * Created by roman on 04.12.16.
 */

@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.Component> {

    private ProductRealm mProductRealm;

    private int mProductCardState = 0;

    public ProductScreen(ProductRealm product) {
        mProductRealm = product;
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component.builder()
                .module(new Module())
                .component(parentComponent)
                .build();
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof ProductScreen) && mProductRealm.equals(((ProductScreen) o).mProductRealm);
    }

    @Override
    public int hashCode() {
        return mProductRealm.hashCode();
    }

    //region ======================== DI =========================

    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductRealm);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @ProductScope
    public interface Component {
        void inject(ProductPresenter presenter);

        void inject(ProductView view);
    }

    //endregion

    //region ===================== Presenter =====================

    public class ProductPresenter extends AbstractPresenter<ProductView, CatalogModel> implements IProductPresenter {

        @Inject
        RefWatcher mRefWatcher;

        private ProductRealm mProduct;
        private RealmChangeListener mListener;

        public ProductPresenter(ProductRealm productRealm) {
            mProduct = productRealm;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null && mProduct.isValid()) {
                getView().showProductView(new ProductDto(mProduct));

                mListener = element -> {
                    if (getView() != null) {
                        getView().showProductView(new ProductDto(mProduct));
                    }
                };
                mProduct.addChangeListener(mListener);
            } else {
                // TODO: 09.02.17 implement me
            }
        }

        @Override
        public void dropView(ProductView view) {
            mProduct.removeChangeListener(mListener);
            super.dropView(view);
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initActionBar() {
            // empty
        }

        @Override
        protected void onExitScope() {
            mRefWatcher.watch(this);
            super.onExitScope();
        }

        @Override
        public void clickOnPlus() {
            if (getView() != null) {
                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(realm1 -> mProduct.add());
                realm.close();
            }
        }

        @Override
        public void clickOnMinus() {
            if (getView() != null) {
                if (mProduct.getCount() > 0) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(realm1 -> mProduct.remove());
                    realm.close();
                }
            }
        }

        public void clickFavorite() {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> mProduct.changeFavorite());
            realm.close();
        }

        public void clickShowMore() {
            Flow.get(getView()).set(new DetailScreen(mProduct));
        }

        public int getProductCardState() {
            return mProductCardState;
        }

        public void setProductCardState(int state) {
            mProductCardState = state;
        }

    }

    //endregion
}
