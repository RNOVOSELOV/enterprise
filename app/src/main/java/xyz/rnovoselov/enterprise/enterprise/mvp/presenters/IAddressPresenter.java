package xyz.rnovoselov.enterprise.enterprise.mvp.presenters;

/**
 * Created by roman on 07.12.16.
 */

public interface IAddressPresenter {

    void clickOnAddAddress();
}
