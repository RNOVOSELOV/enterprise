package xyz.rnovoselov.enterprise.enterprise.utils;

import xyz.rnovoselov.enterprise.enterprise.BuildConfig;

/**
 * Created by roman on 17.02.17.
 */

public interface ConstantManager {
    String TAG_PREFIX = "ENTERPRISE_";

    String PROGRESS_DIALOG_SHOWN = "PROGRESS_DIALOG_SHOWN_FLAG";

    String FILE_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".provider";

    int REQUEST_PROFILE_PHOTO_PICKER = 1000;
    int REQUEST_PROFILE_PHOTO_CAMERA = 1001;

    int REQUEST_PERMISSION_CAMERA = 2000;
    int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 2001;

    String HEADER_LAST_MODIFIED = "Last-Modified";
    String HEADER_IF_MODIFIED_SINCE = "If-Modified-Since";

    String REALM_USER = "testrealm@mail.ru";
    String REALM_PASSWORD = "11111111";
    String REALM_AUTH_URL = "http://192.168.1.73:9080/auth";
    String REALM_DB_URL = "realm://192.168.1.73:9080/~/default";
}