package xyz.rnovoselov.enterprise.enterprise.utils;

/**
 * Created by roman on 22.10.16.
 */

public interface AppConfig {
    String BASE_URL = "https://skba1.mgbeta.ru/api/v1/";
    int MAX_CONNECTION_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;
    int MAX_WRITE_TIMEOUT = 5000;
}
